﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameStorer : MonoBehaviour {
    public string datapath;
    public InputField inputField;
    const string FILENAME = "/PlayerName.txt";
    // Use this for initialization
    void Start () {
        datapath = Application.persistentDataPath + FILENAME;
        if (File.Exists(datapath))
        {
            var reader = new StreamReader(datapath);
            inputField.text = reader.ReadLine();
            reader.Close();
        }
        else
        {
            StreamWriter sw = new StreamWriter(datapath);
            sw.WriteLine("Unknown");
            sw.Close();
        }

	}

    public void OnEndEdit()
    {
        if (!string.IsNullOrEmpty(inputField.text))
        {
            if (File.Exists(datapath))
            {
                File.Delete(datapath);
            }
            StreamWriter sw = new StreamWriter(datapath);
            sw.WriteLine(inputField.text);
            sw.Close();
        }
        else
        {
            if (File.Exists(datapath))
            {
                File.Delete(datapath);
            }
            StreamWriter sw = new StreamWriter(datapath);
            sw.WriteLine("Unknown");
            sw.Close();
        }
    }

    public static string GetPlayerName()
    {
        var reader = new StreamReader(Application.persistentDataPath + FILENAME);
        var r = reader.ReadLine();
        reader.Close();
        return r;
    }
}
