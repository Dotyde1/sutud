﻿using System.Collections.Generic;
using UnityEngine;

namespace Sutud.Units.UnitStates
{
    [CreateAssetMenu(fileName ="NewUnitState", menuName ="Unit state")]
    public class UnitState : ScriptableObject
    {
    }
}