﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;
using System.Linq;

public struct Score
{
    public Score(String name, int score)
    {
        playerName = name;
        playerScore = score;
    }

    public String playerName;
    public int playerScore;
}

public class Highscore : MonoBehaviour
{
    private string connectionString;
    
    void Start ()
    {
        connectionString = "URI=file:" + Application.streamingAssetsPath + "/Highscore.db";
    }
	
	public void AddHighscore(String playerName, int playerScore)
    {
        connectionString = "URI=file:" + Application.streamingAssetsPath + "/Highscore.db";

        using (var dbconn = (IDbConnection) new SqliteConnection(connectionString))
	    {
            dbconn.Open();  
            using (IDbCommand dbcmd = dbconn.CreateCommand())
	        {
	            string sqlQuery = "INSERT INTO Highscore (Name, Score) VALUES('" + playerName + "', '" +
	                              playerScore.ToString() +
	                              "');";
	            dbcmd.CommandText = sqlQuery;

	            Debug.Log(dbcmd.ExecuteNonQuery());
	        }
            dbconn.Close();
	    }
    }

    public List<Score> GetHighscore()
    {
        List<Score> score = new List<Score>();

        if (connectionString == null)
        {
            connectionString = "URI=file:" + Application.streamingAssetsPath + "/Highscore.db";
        }

        using (var dbconn = (IDbConnection) new SqliteConnection(connectionString))
        {
            dbconn.Open();
            IDbCommand dbcmd = dbconn.CreateCommand();
            string sqlQuery = "SELECT Name, Score FROM Highscore";
            dbcmd.CommandText = sqlQuery;
            using (IDataReader reader = dbcmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    score.Add(new Score(reader.GetString(0), reader.GetInt32(1)));
                }
                reader.Close();
            }
            dbconn.Close();
        }
        
        return score.OrderByDescending(o => o.playerScore).ToList();
    }
}
