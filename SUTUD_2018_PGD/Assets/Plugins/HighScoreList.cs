﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreList : MonoBehaviour
{
    public GameObject HighScoreItem;
    private Highscore highscore;
    const int MAXDISP = 10;

    // Use this for initialization
    void Start ()
    {
        if (highscore == null)
        {
            highscore = new Highscore();
        }

        var scores = highscore.GetHighscore();

        int height = 0;
        int disp = 0;
        foreach (var score in scores)
        {
            height -= 130;

            var newItem = Instantiate(HighScoreItem);
            newItem.transform.position += new Vector3(0, height, 0);
            newItem.transform.SetParent(gameObject.transform, false);

            newItem.GetComponent<Text>().text = "Player: " + score.playerName + ", Wave: " + score.playerScore;
            if (++disp > MAXDISP)
                break;
        }
    }
}
