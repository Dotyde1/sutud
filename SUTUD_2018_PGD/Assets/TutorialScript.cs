﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sutud.Cameras;
using Sutud.Events;
namespace Sutud.Cameras{
	public class TutorialScript : MonoBehaviour {
		[SerializeField]
		private Text tutorialText;

		private int numberNextButtonPressed = 0;
		[SerializeField]

		private Button quitButton;
		[SerializeField]
		private Button nextButton;
		[SerializeField]

		private string[] tutorialQuotes = new string[]{
			"Welcome To SUTUD a game all about kicking name and taking ass... no wait that's wrong.\n" + "Anyhow in this tutorial we will explain all the basics of the game to you.",
			"This is where the magic happens, here you will buy units and build your army. Now let's start with those units.",
			"Here you see the 5 units that you will be using plus the incomebuilding to the left. First we'll show you some of the units.",
			"The melee unit, a fairly basic unit who can act as you front line. Hover over a unit to see it's stats in the lower left corner.",
			"This is the income building, it increases the amount of money you get after each wave. Use this if you want to go for a long term strategy.", 
			"Now that you know the basics about units, go and drag some of them on the buildplatform, but pay attention to your gold, seen in the top left corner",
			"You now know the basic of SUTUD, march forth en conquer your opponents. Good Luck."
		};

		public GameEvent OnfightEnd;
		public GameEvent ShowUnits;
		public GameEvent ShowMelee;
		public GameEvent ShowBuilding;
		public GameEvent PlaceUnit;
		// Use this for initialization
		void Awake () {
			quitButton.interactable = false;
		}
		
		// Update is called once per frame
		void Update () {
			switch (numberNextButtonPressed) {
			case 0:
				tutorialText.text = tutorialQuotes [0];
				break;
			case 1:
				tutorialText.text = tutorialQuotes [1];
				OnfightEnd.Raise ();
				break;
			case 2:
				tutorialText.text = tutorialQuotes [2];
				ShowUnits.Raise ();
				break;
			case 3:
				tutorialText.text = tutorialQuotes [3];
				ShowMelee.Raise ();
				break;
			case 4:
				tutorialText.text = tutorialQuotes [4];
				ShowBuilding.Raise ();
				break;
			case 5:
				tutorialText.text = tutorialQuotes [5];
				PlaceUnit.Raise ();
				break;
			case 6:
				tutorialText.text = tutorialQuotes [6];
				PlaceUnit.Raise ();
				quitButton.interactable = true;
				nextButton.interactable = false;
				break;
			}
		}

		public void NextIsPressed(){
			numberNextButtonPressed += 1;
		}
	}
}
