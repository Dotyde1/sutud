﻿using UnityEditor;
using UnityEngine;
using System;
using System.Text;
using System.Collections.Generic;

[Flags]
public enum EditorListOption
{
    None = 0,
    ListSize = 1,
    ListLabel = 2,
    ElementLabels = 4,
    Buttons = 8,
    NamedElementLabels = 16,
    Default = ListSize | ListLabel | ElementLabels,
    NoElementLabels = ListSize | ListLabel,
    All = Default | Buttons
}

public static class EditorList
{

    private static GUIContent
        moveButtonContent = new GUIContent("\u2193", "move down"),
        duplicateButtonContent = new GUIContent("+", "duplicate"),
        deleteButtonContent = new GUIContent("-", "delete"),
        clearButtonContent = new GUIContent("Clear", "Clear the list");


    private static GUILayoutOption miniButtonWidth = GUILayout.Width(20f);

    public static void Show(SerializedProperty list, EditorListOption options = EditorListOption.Default)
    {
        if(list == null)
        {
            EditorGUILayout.HelpBox("The serialized property provided is null!", MessageType.Error);
            return;
        }
        if(!list.isArray)
        {
            EditorGUILayout.HelpBox(list.name + " is neither an array nor a list!", MessageType.Error);
            return;
        }

        bool showListLabel = (options & EditorListOption.ListLabel) != 0,
            showListSize = (options & EditorListOption.ListSize) != 0;

        if(showListLabel)
        {
            EditorGUILayout.PropertyField(list);
            EditorGUI.indentLevel += 1;
        }
        if(!showListLabel || list.isExpanded)
        {
            SerializedProperty size = list.FindPropertyRelative("Array.size");
            if(showListSize)
            {
                EditorGUILayout.PropertyField(size);
            }
            if(size.hasMultipleDifferentValues)
            {
                EditorGUILayout.HelpBox("Not showing lists with different sizes.", MessageType.Info);
            }
            else
            {
                ShowElements(list, options);
            }
        }
        if(showListLabel)
        {
            EditorGUI.indentLevel -= 1;
        }
    }

    private static void ShowElements(SerializedProperty list, EditorListOption options)
    {
        bool showElementLabels = (options & EditorListOption.ElementLabels) != 0,
            showButtons = (options & EditorListOption.Buttons) != 0,
            namedElementLabels = (options & EditorListOption.NamedElementLabels) != 0;

        int digitCount = (int)Math.Floor(Math.Log10(list.arraySize - 1) + 1);
        if(digitCount < 1)
            digitCount = 1;
        string numberFormat = "[{0," + digitCount + "}]";
        for(int i = 0; i < list.arraySize; i++)
        {
            GUIContent content = new GUIContent();
            if(showButtons)
            {
                EditorGUILayout.BeginHorizontal();
            }
            if(showElementLabels)
            {
                if (namedElementLabels)
                {
                    var elementAtI = list.GetArrayElementAtIndex(i);
                    if (elementAtI != null || elementAtI.objectReferenceValue != null)
                    {
                        EditorGUILayout.BeginHorizontal();
						try {
							GUILayout.Label(string.Format(numberFormat + " {1}", i, list.GetArrayElementAtIndex(i).objectReferenceValue.name));
						} catch (Exception){
							GUILayout.Label(string.Format(numberFormat + " {1}", i, null));
						}
                        EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), GUIContent.none, GUILayout.Width(150f));
                        EditorGUILayout.EndHorizontal();
                    }
                    else
                        content.text = string.Format(numberFormat + " {1}", i, "Null");
                }
                else
                {
                    content.text = string.Format(numberFormat, i);
                    EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), content);
                }
            }
            else
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), content);

            if(showButtons)
            {
                ShowButtons(list, i);
                EditorGUILayout.EndHorizontal();
            }
        }
        if(showButtons)
        {
            GUILayout.BeginHorizontal();
            if(GUILayout.Button(clearButtonContent))
                list.ClearArray();
            if(GUILayout.Button("Add empty element"))
            {
                list.arraySize++;
                switch(list.GetArrayElementAtIndex(list.arraySize - 1).propertyType)
                {
                    case SerializedPropertyType.AnimationCurve:
                    case SerializedPropertyType.Color:
                    case SerializedPropertyType.ExposedReference:
                    case SerializedPropertyType.FixedBufferSize:
                    case SerializedPropertyType.Generic:
                    case SerializedPropertyType.Gradient:
                    case SerializedPropertyType.LayerMask:
                    case SerializedPropertyType.ObjectReference:
                    case SerializedPropertyType.Quaternion:
                    case SerializedPropertyType.Rect:
                        list.GetArrayElementAtIndex(list.arraySize - 1).objectReferenceValue = null;
                        break;
                    case SerializedPropertyType.Float:
                        list.GetArrayElementAtIndex(list.arraySize - 1).floatValue = 0f;
                        break;
                    default:
                        Debug.Log("Need to still implement behavior for " + list.GetArrayElementAtIndex(list.arraySize - 1).propertyType);
                        break;
                }
            }
            GUI.enabled = list.arraySize > 0;
            if(GUILayout.Button("Remove bottom element"))
                list.arraySize--;
            GUI.enabled = true;
            GUILayout.EndHorizontal();
        }
    }

    private static void ShowButtons(SerializedProperty list, int index)
    {
        if(GUILayout.Button(moveButtonContent, EditorStyles.miniButtonLeft, miniButtonWidth))
        {
            list.MoveArrayElement(index, index + 1);
        }
        if(GUILayout.Button(duplicateButtonContent, EditorStyles.miniButtonMid, miniButtonWidth))
        {
            list.InsertArrayElementAtIndex(index);
        }
        if(GUILayout.Button(deleteButtonContent, EditorStyles.miniButtonRight, miniButtonWidth))
        {
            int oldSize = list.arraySize;
            list.DeleteArrayElementAtIndex(index);
            if(list.arraySize == oldSize)
            {
                list.DeleteArrayElementAtIndex(index);
            }
        }
    }
}