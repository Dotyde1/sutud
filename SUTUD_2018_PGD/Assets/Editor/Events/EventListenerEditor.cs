﻿using UnityEditor;
using UnityEngine;

namespace Sutud.Events.Editors
{
    [CustomEditor(typeof(GameEventListener))]
    public class EventListenerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GameEventListener listener = target as GameEventListener;
            GameEvent e = listener.Event;

            GUI.enabled = Application.isPlaying && e != null;

            if(GUILayout.Button("Raise"))
                e.Raise();

            GUI.enabled = Application.isPlaying;

            if(GUILayout.Button("Raise for this listener only"))
                listener.OnEventRaised();
        }
    }
}