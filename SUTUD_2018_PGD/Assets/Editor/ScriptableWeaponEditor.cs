﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sutud.Variables;
using Sutud.Units.Behaviors;

namespace Sutud.Units.Weapons.Editors
{
    [CustomEditor(typeof(ScriptableWeapon))]
    public class ScriptableWeaponEditor : Editor
    {
        SerializedProperty PrefabsThatUseThisWeapon;
        SerializedProperty weaponUiArt;
        SerializedProperty weaponName;
        SerializedProperty damageRange;
        SerializedProperty weaponRange;
        SerializedProperty hasMinimumRange;
        SerializedProperty minimumRange;
        SerializedProperty canAttackTypes;
        SerializedProperty attacksSpeed;
        SerializedProperty isRanged;
        SerializedProperty hasMultiTarget;
        SerializedProperty numberOfAdditional;
        SerializedProperty multiTargetRange;
        SerializedProperty multiTargetDamageMultiplier;
        SerializedProperty rangedProjectile;
        SerializedProperty usesProjectile;
        SerializedProperty dealsAoeDamage;
        SerializedProperty aoeRange;
        SerializedProperty aoeDamageMultiplier;
        SerializedProperty aoeDamageSpreadMode;
        SerializedProperty aoeSpreadCurve;
        SerializedProperty attackAnimation;
        SerializedProperty critAnimation;
        SerializedProperty critChance;
        SerializedProperty attackSound;
        SerializedProperty critSound;
        SerializedProperty critDamageMultiplier;
        SerializedProperty weaponFlavorText;
        Material mat;
        public int aoeDrawerDetail = 10;
        SerializedProperty aoETesterLocations;
        public float totalAoeDamage = 0;
        public float test = 0;
        CanTargetTypes canTargetEditor;
        bool genericSettings = true, rangedSettings = true,
            AoESettings = true, critSettings = true,
            multiTargetSettings = true, debugViews = true,
            animationSettings = true, soundSettings = true;


        void OnEnable()
        {
            PrefabsThatUseThisWeapon = serializedObject.FindProperty("PrefabsThatUseThisWeapon");
            weaponUiArt = serializedObject.FindProperty("weaponUiArt");
            weaponName = serializedObject.FindProperty("weaponName");
            damageRange = serializedObject.FindProperty("damageRange");
            weaponRange = serializedObject.FindProperty("weaponRange");
            hasMinimumRange = serializedObject.FindProperty("hasMinimumRange");
            minimumRange = serializedObject.FindProperty("minimumRange");
            canAttackTypes = serializedObject.FindProperty("canAttackTypes");
            attacksSpeed = serializedObject.FindProperty("attacksSpeed");
            isRanged = serializedObject.FindProperty("isRanged");
            hasMultiTarget = serializedObject.FindProperty("hasMultiTarget");
            numberOfAdditional = serializedObject.FindProperty("numberOfAdditional");
            multiTargetDamageMultiplier = serializedObject.FindProperty("multiTargetDamageMultiplier");
            multiTargetRange = serializedObject.FindProperty("multiTargetRange");
            rangedProjectile = serializedObject.FindProperty("rangedProjectile");
            usesProjectile = serializedObject.FindProperty("usesProjectile");
            dealsAoeDamage = serializedObject.FindProperty("dealsAoeDamage");
            aoeRange = serializedObject.FindProperty("aoeRange");
            aoeDamageMultiplier = serializedObject.FindProperty("aoeDamageMultiplier");
            aoeDamageSpreadMode = serializedObject.FindProperty("aoeDamageSpreadMode");
            aoeSpreadCurve = serializedObject.FindProperty("aoeSpreadCurve");
            attackAnimation = serializedObject.FindProperty("attackAnimation");
            critAnimation = serializedObject.FindProperty("critAnimation");
            attackSound = serializedObject.FindProperty("attackSound");
            critSound = serializedObject.FindProperty("critSound");
            critChance = serializedObject.FindProperty("critChance");
            critDamageMultiplier = serializedObject.FindProperty("critDamageMultiplier");
            weaponFlavorText = serializedObject.FindProperty("weaponFlavorText");
            aoETesterLocations = serializedObject.FindProperty("AreaOfEffectTesters");
            var shader = Shader.Find("Hidden/Internal-Colored");
            mat = new Material(shader);
        }

        public override void OnInspectorGUI()
        {
            EditorStyles.textField.wordWrap = true;
            serializedObject.Update();
            GUI.enabled = false;
			for (int i = PrefabsThatUseThisWeapon.arraySize - 1; i > 0; i--) {
				var weaponBehaviour = PrefabsThatUseThisWeapon.GetArrayElementAtIndex (i);
				if (weaponBehaviour == null) {
					PrefabsThatUseThisWeapon.DeleteArrayElementAtIndex (i);
				}
			}
            EditorList.Show(PrefabsThatUseThisWeapon, EditorListOption.NamedElementLabels | EditorListOption.ListSize | EditorListOption.ListLabel | EditorListOption.ElementLabels);
            GUI.enabled = true;
            EditorGUI.BeginChangeCheck();
            genericSettings = EditorGUILayout.Foldout(genericSettings,new GUIContent("Generic weapon settings"));
            if (genericSettings)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(weaponUiArt);
                EditorGUILayout.PropertyField(weaponName);
                EditorGUILayout.PropertyField(damageRange);
                EditorGUILayout.PropertyField(weaponRange);
                if (weaponRange.floatValue < 0f)
                {
                    weaponRange.floatValue = 0;
                }
                EditorGUILayout.PropertyField(hasMinimumRange);
                if (hasMinimumRange.boolValue)
                {
                    EditorGUILayout.PropertyField(minimumRange);
                    if (weaponRange.floatValue - minimumRange.floatValue < 0)
                    {
                        minimumRange.floatValue = weaponRange.floatValue;
                    }
                }

                EditorGUILayout.PropertyField(attacksSpeed, new GUIContent("Attacks per second", "The number of attacks this weapon will fire per second"));
                if (attacksSpeed.floatValue <= 0f)
                {
                    EditorGUILayout.HelpBox("A weapons attackspeed must atleast be larger then 0", MessageType.Error);
                    attacksSpeed.floatValue = 0f;
                }
                canTargetEditor = (CanTargetTypes)EditorGUILayout.EnumFlagsField("Can Attack types", canTargetEditor);
                canAttackTypes.intValue = (int)canTargetEditor;
                EditorGUI.indentLevel--;
            }
            rangedSettings = EditorGUILayout.Foldout(rangedSettings, new GUIContent("Ranged weapon settings"));
            if (rangedSettings)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(isRanged);
                if (isRanged.boolValue)
                {
                    EditorGUILayout.PropertyField(usesProjectile);
                    EditorGUILayout.EndHorizontal();
                    if (usesProjectile.boolValue)
                    {
                        EditorGUILayout.PropertyField(rangedProjectile);
                        GameObject projectileObject = rangedProjectile.objectReferenceValue as GameObject;
                        if (rangedProjectile.objectReferenceValue == null)
                            EditorGUILayout.HelpBox("A weapon that uses a projectileObject MUST not have a null projectile field", MessageType.Error);
                        else if (projectileObject.GetComponent<Projectiles.BaseProjectile>() == null)
                            EditorGUILayout.HelpBox("A projectileObject MUST have a Projectile monobehavior attached", MessageType.Error);
                    }
                    multiTargetSettings = EditorGUILayout.Foldout(multiTargetSettings, new GUIContent("Multi target weapon settings"));
                    if (multiTargetSettings)
                    {
                        EditorGUI.indentLevel++;
                        EditorGUILayout.PropertyField(hasMultiTarget);
                        EditorGUI.indentLevel--;
                    }
                }
                if (!isRanged.boolValue)
                    EditorGUILayout.EndHorizontal();


                if (hasMultiTarget.boolValue && isRanged.boolValue)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.PropertyField(numberOfAdditional);
                    EditorGUILayout.PropertyField(multiTargetRange);
                    EditorGUILayout.PropertyField(multiTargetDamageMultiplier);
                    EditorGUI.indentLevel--;
                }
                EditorGUI.indentLevel--;
            }

            AoESettings = EditorGUILayout.Foldout(AoESettings, new GUIContent("Area of effect weapon settings"));
            if (AoESettings)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(dealsAoeDamage);
                if (dealsAoeDamage.boolValue)
                {
                    EditorGUILayout.PropertyField(aoeRange);
                    if (aoeRange.floatValue <= 0f)
                    {
                        EditorGUILayout.HelpBox("A weapons Aoe should probably be larger then 0", MessageType.Warning);
                        aoeRange.floatValue = 0;
                    }
                    EditorGUILayout.PropertyField(aoeDamageMultiplier);
                    EditorGUILayout.PropertyField(aoeDamageSpreadMode);
                    if (aoeDamageSpreadMode.enumValueIndex == (int)AoeDamageSpreadMode.CurveEditor)
                    {
                        EditorGUILayout.CurveField(aoeSpreadCurve, Color.green, new Rect(0, 0, 1, 1));
                    }
                }
                EditorGUI.indentLevel--;
            }
            /*for (float i = 0; i <= 1; i += .1f) {
                var valAtI = aoeSpreadCurve.animationCurveValue.Evaluate(i);
                if (valAtI < 0 || valAtI > 1)
                    EditorGUILayout.HelpBox("Value at " + i + " in the curve is invalid! Value will be clamped between 0 and 1", MessageType.Warning);
            }*///This shit doesn't work but don't know why?
            animationSettings = EditorGUILayout.Foldout(animationSettings, new GUIContent("Animations weapon settings"));
            if (animationSettings)
            {
                EditorGUI.indentLevel++;
                EditorList.Show(attackAnimation, EditorListOption.All);
                EditorList.Show(critAnimation, EditorListOption.All);
                EditorGUI.indentLevel--;
            }
            soundSettings = EditorGUILayout.Foldout(soundSettings, new GUIContent("Sounds weapon settings"));
            if (soundSettings)
            {
                EditorGUI.indentLevel++;
                EditorList.Show(attackSound, EditorListOption.All);
                EditorList.Show(critSound, EditorListOption.All);
                EditorGUI.indentLevel--;
            }

            critSettings = EditorGUILayout.Foldout(critSettings, new GUIContent("Crit weapon settings"));
            if (critSettings)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(critChance);
                EditorGUILayout.PropertyField(critDamageMultiplier);
                EditorGUILayout.PropertyField(weaponFlavorText);
                EditorGUI.indentLevel--;
            }
            if (EditorGUI.EndChangeCheck())
            {
                var Target = target as ScriptableWeapon;
                foreach (UseWeaponsBehavior weaponBehavior in Target.PrefabsThatUseThisWeapon)
                {
					if (weaponBehavior != null) {
						weaponBehavior.BakeWeapons ();
					}
                }
            }
            debugViews = EditorGUILayout.Foldout(debugViews, new GUIContent("Debug views"));
            if (debugViews)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.HelpBox("Below are some helpfull variables that roll out of this weapons settings :)", MessageType.Info);
                //Display esitamted DPS shit next
                float minDamage = damageRange.FindPropertyRelative("minValue").floatValue;
                float maxDamage = damageRange.FindPropertyRelative("maxValue").floatValue;
                float midDamage = (minDamage + maxDamage) / 2;

                float estimatedDps = midDamage * attacksSpeed.floatValue;
                float averageCritDamage = estimatedDps * critChance.floatValue * critDamageMultiplier.floatValue;

                GUI.enabled = false;
                EditorGUILayout.FloatField(
                    new GUIContent("Estimated single target dps", "The estimated dps not taking Crit, AoE or Multi target into account at all")
                    , estimatedDps);
                EditorGUILayout.FloatField(
                    new GUIContent("Estimated single target critDamage", "The estimated damage that is derived purely from crit")
                    , averageCritDamage);
                EditorGUILayout.FloatField(
                    new GUIContent("Estimated total single target Damage", "The estimated damage as a result of adding crit and the normal damage")
                    , estimatedDps + averageCritDamage);
                if (dealsAoeDamage.boolValue)
                {
                    GUI.enabled = true;
                    EditorGUILayout.HelpBox("AoE Damage drawer is shown below", MessageType.Info);
                    EditorList.Show(aoETesterLocations, EditorListOption.All);

                    aoeDrawerDetail = EditorGUI.IntSlider(GUILayoutUtility.GetRect(10, 1000, 20, 20), new GUIContent("Aoe drawer detail"), aoeDrawerDetail, 3, 50);
                    GUI.enabled = false;

                    EditorGUILayout.FloatField(
                        new GUIContent("Estimated aoe damage", "Estimated aoe damage given what the user provided we would hit now.")
                        , totalAoeDamage);
                    EditorGUILayout.FloatField(
                        new GUIContent("Average aoe damage per target", "The estimated aoe damage given what the user provided, divded by the number of targets")
                        , totalAoeDamage / aoETesterLocations.arraySize);

                    

                    Rect rect = GUILayoutUtility.GetRect(10, 1000, 350, 350);
                    if (Event.current.type == EventType.Repaint)
                    {
                        GUI.BeginClip(rect);
                        GL.PushMatrix();
                        GL.Clear(true, false, Color.black);
                        mat.SetPass(0);

                        // background
                        GL.Begin(GL.QUADS);
                        GL.Color(Color.black);
                        GL.Vertex3(0, 0, 0);
                        GL.Vertex3(rect.width, 0, 0);
                        GL.Vertex3(rect.width, rect.height, 0);
                        GL.Vertex3(0, rect.height, 0);
                        GL.End();

                        var width = rect.width / (aoeDrawerDetail * 2);
                        //draw AOE damage fields visualization
                        for (int i = 0; i < aoeDrawerDetail; i++)
                        {
                            float eval = 0;
                            switch (aoeDamageSpreadMode.enumValueIndex)
                            {
                                case (int)AoeDamageSpreadMode.CurveEditor:
                                    eval = aoeSpreadCurve.animationCurveValue.Evaluate((float)i / aoeDrawerDetail);
                                    break;
                                case (int)AoeDamageSpreadMode.Constant:
                                    eval = 1;
                                    break;
                                case (int)AoeDamageSpreadMode.Linear:
                                    eval = (float)i / aoeDrawerDetail;
                                    break;
                            }
                            Color color = new Color(eval, .2f, .3f);

                            //DrawGLCircle(radius, radius + width, color, 100, rect);
                            var radiusDowner = (width * aoeDrawerDetail) + width * -i; ;
                            DrawGLCircle(radiusDowner - width, radiusDowner, color, 15 * aoeDrawerDetail, rect);
                        }

                        totalAoeDamage = 0;
                        for (int i = 0; i < aoETesterLocations.arraySize; i++)
                        {
                            float aoeTester = aoETesterLocations.GetArrayElementAtIndex(i).floatValue;

                            float eval = 0;
                            switch (aoeDamageSpreadMode.enumValueIndex)
                            {
                                case (int)AoeDamageSpreadMode.CurveEditor:
                                    eval = 1 - aoeSpreadCurve.animationCurveValue.Evaluate(aoeTester);
                                    break;
                                case (int)AoeDamageSpreadMode.Constant:
                                    eval = 1;
                                    break;
                                case (int)AoeDamageSpreadMode.Linear:
                                    eval = 1 - aoeTester;
                                    break;
                            }

                            DrawGLCircle(rect.width / 2 * Mathf.Clamp(aoeTester, 0.01f, 1), Color.yellow, (int)(80 * (1 - eval)) + 10, rect);

                            float aoeDamage = eval * midDamage * aoeDamageMultiplier.floatValue;
                            totalAoeDamage += aoeDamage;
                        }

                        GL.PopMatrix();

                        // draw grid
                        GL.Begin(GL.LINES);
                        int count = (int)(rect.width / 10) + 20;
                        for (int i = 0; i < count; i++)
                        {
                            float f = (i % 5 == 0) ? 0.5f : 0.2f;
                            GL.Color(new Color(f, f, f, 1));
                            float x = i * 10;
                            if (x >= 0 && x < rect.width)
                            {
                                GL.Vertex3(x, 0, 0);
                                GL.Vertex3(x, rect.height, 0);
                            }
                            if (i < rect.height / 10)
                            {
                                GL.Vertex3(0, i * 10, 0);
                                GL.Vertex3(rect.width, i * 10, 0);
                            }
                        }
                        GL.End();

                        GUI.EndClip();
                    }
                }
                EditorGUI.indentLevel--;
            }


            serializedObject.ApplyModifiedProperties();
        }

        public void DrawGLCircle(float circleRadius, Color color, int detail, Rect drawingArea)
        {
            float degreesPerIndex = 2 * Mathf.PI / detail;
            Vector2 center = drawingArea.center;
            center -= new Vector2(drawingArea.xMin, drawingArea.yMin);

            GL.Begin(GL.LINES);
            GL.Color(color);
            for(int i = 0; i < detail; i++)
            {
                Vector2 offset = new Vector2(Mathf.Sin(degreesPerIndex * i), Mathf.Cos(degreesPerIndex * i)) * circleRadius;
                Vector2 nextOffset = new Vector2(Mathf.Sin(degreesPerIndex * (1 + i)), Mathf.Cos(degreesPerIndex * (1 + i))) * circleRadius;
                if (!(drawingArea.Contains(drawingArea.center + offset) && drawingArea.Contains(drawingArea.center + nextOffset)))
                    continue;

                GL.Vertex(center + offset);
                GL.Vertex(center + nextOffset);
            }
            GL.End();
        }

        public void DrawGLCircle(float circleRadiusMin, float circleRadiusMax, Color color, int detail, Rect drawingArea)
        {
            float degreesPerIndex = 2 * Mathf.PI / detail;
            Vector2 center = drawingArea.center;
            center -= drawingArea.min;

            GL.Begin(GL.TRIANGLES);
            GL.Color(color);
            if(circleRadiusMin == 0)
            {
                for(int i = 0; i < detail; i++)
                {
                    Vector2 offsetMin = new Vector2(Mathf.Sin(degreesPerIndex * (i + .5f)), Mathf.Cos(degreesPerIndex * (i + .5f))) * circleRadiusMin;
                    Vector2 offset = new Vector2(Mathf.Sin(degreesPerIndex * i), Mathf.Cos(degreesPerIndex * i)) * circleRadiusMax;
                    Vector2 nextOffset = new Vector2(Mathf.Sin(degreesPerIndex * (1 + i)), Mathf.Cos(degreesPerIndex * (1 + i))) * circleRadiusMax;
                    if(drawingArea.Contains(offsetMin) || drawingArea.Contains(offset) || drawingArea.Contains(nextOffset))
                        continue;
                    GL.Vertex(center + offsetMin);
                    GL.Vertex(center + offset);
                    GL.Vertex(center + nextOffset);
                }
            }
            else
            {
                for(int i = 0; i < detail; i++)
                {
                    Vector2 offsetMinT1 = new Vector2(Mathf.Sin(degreesPerIndex * (i + .5f)), Mathf.Cos(degreesPerIndex * (i + .5f))) * circleRadiusMin;
                    Vector2 curentOffset = new Vector2(Mathf.Sin(degreesPerIndex * i), Mathf.Cos(degreesPerIndex * i)) * circleRadiusMax;
                    Vector2 nextOffsetT1 = new Vector2(Mathf.Sin(degreesPerIndex * (1 + i)), Mathf.Cos(degreesPerIndex * (1 + i))) * circleRadiusMax;
                    if (curentOffset.x + center.x < 0 || curentOffset.y + center.y < 0|| curentOffset.x + center.x > drawingArea.width || curentOffset.y + center.y > drawingArea.height ||
                        nextOffsetT1.x + center.x < 0 || nextOffsetT1.y + center.y < 0 || nextOffsetT1.x + center.x > drawingArea.width || nextOffsetT1.y + center.y > drawingArea.height)
                        continue;
                    GL.Vertex(center + offsetMinT1);
                    GL.Vertex(center + curentOffset);
                    GL.Vertex(center + nextOffsetT1);
                    float j = i + 0.5f;
                    Vector2 offsetMinT2 = new Vector2(Mathf.Sin(degreesPerIndex * (j + .5f)), Mathf.Cos(degreesPerIndex * (j + .5f))) * circleRadiusMax;
                    //Vector2 offsetT2 = new Vector2(Mathf.Sin(degreesPerIndex * j), Mathf.Cos(degreesPerIndex * j)) * circleRadiusMin;
                    Vector2 nextOffsetT2 = new Vector2(Mathf.Sin(degreesPerIndex * (1 + j)), Mathf.Cos(degreesPerIndex * (1 + j))) * circleRadiusMin;
                    GL.Vertex(center + offsetMinT2);
                    GL.Vertex(center + offsetMinT1);
                    GL.Vertex(center + nextOffsetT2);
                }
            }
            GL.End();
        }
    }
}