﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sutud.Units.Behaviors;

[CustomEditor(typeof(UseWeaponsBehavior))]
[CanEditMultipleObjects]
public class UseWeaponBehaviorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        base.OnInspectorGUI();
        if (EditorGUI.EndChangeCheck())
        {
            foreach(UseWeaponsBehavior t in targets)
            {
                t.BakeWeapons();
            } 
        }
    }
}