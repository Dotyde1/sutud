﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sutud.Units.Behaviors;
using Sutud.Units.Properties;
namespace Sutud.Units.EditorScripts
{
    [CustomEditor(typeof(Unit))]
    [CanEditMultipleObjects]
    public class BakeUnitButton : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Bake unit references"))
            {
                foreach (Unit unit in targets)
                {
                    var behaviorManager = unit.GetComponent<UnitBehaviourManager>();
                    var propMananger = unit.GetComponent<UnitPropertiesManager>();
                    var allBehaviors = unit.GetComponents<UnitBehaviour>();
                    unit.Bake();
                    EditorUtility.SetDirty(unit);
                    EditorUtility.SetDirty(behaviorManager);
                    EditorUtility.SetDirty(propMananger);
                    foreach(UnitBehaviour unitBehavior in allBehaviors)
                    {
                        EditorUtility.SetDirty(unitBehavior);
                    }
                }
            }
            if (GUILayout.Button("Un-Bake unit references"))
            {
                foreach (Unit unit in targets)
                {
                    var behaviorManager = unit.GetComponent<UnitBehaviourManager>();
                    var propMananger = unit.GetComponent<UnitPropertiesManager>();
                    var allBehaviors = unit.GetComponents<UnitBehaviour>();
                    unit.UnBake();

                    EditorUtility.SetDirty(unit);
                    EditorUtility.SetDirty(behaviorManager);
                    EditorUtility.SetDirty(propMananger);
                    foreach(UnitBehaviour unitBehavior in allBehaviors)
                    {
                        EditorUtility.SetDirty(unitBehavior);
                    }
                }
            }
        }
    }
}