﻿using Sutud.Variables;
using Sutud.Variables.ReferenceVariables;
using UnityEngine;

public class ReferenceHolder : MonoBehaviour
{
    [Header("1")] public CharReference charthing;
    [Header("2")] public DoubleReference doublething;
    [Header("3")] public FloatReference floatthing;
    [Header("4")] public IntReference intthing;
    [Header("5")] public StringReference stringthing;
    [Header("6")] public Vector3Reference vector3thing;
    [Header("7")] public FloatRangeReference floatRangeThing;

    [Header("RangeVariable Test")]
    public FloatRange aFloatRange;
    public IntRange aIntRange;

    [Header("RangeVariable Test with minmax range")]
    [MinMaxRange(-50, 50)]
    public FloatRange aMinMaxedFloatRange;
    [MinMaxRange(-50, 50)]
    public IntRange aMinMaxedIntRange;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
