﻿using System;

[Serializable]
public class UnitSettings
{
    public string Name = "";
    public string FileLocation = "";
}
