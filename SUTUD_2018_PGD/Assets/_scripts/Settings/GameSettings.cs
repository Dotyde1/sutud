﻿using System;
using System.Collections.Generic;

[Serializable]
public class GameSettings
{
    public List<RaceSettings> Races = new List<RaceSettings>();
}
