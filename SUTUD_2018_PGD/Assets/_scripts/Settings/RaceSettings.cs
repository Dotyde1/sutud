﻿using System;
using System.Collections.Generic;

[Serializable]
public class RaceSettings
{
    public List<UnitSettings> Units = new List<UnitSettings>();
    public string Name = "";
}
