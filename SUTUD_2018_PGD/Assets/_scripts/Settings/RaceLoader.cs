﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//using Boo.Lang.Environments;
using Sutud.Units.Properties;
using UnityEngine;
using Object = UnityEngine.Object;

public class RaceLoader
{
    #region singleton
    private static RaceLoader _instance;

    private RaceLoader() { }

    public static RaceLoader instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new RaceLoader();
                _instance.Load();
            }
            return _instance;
        }
    }
    #endregion

    public Dictionary<Race, List<GameObject>> Units = new Dictionary<Race, List<GameObject>>();

    void Load ()
    {
	    string fileLocation = Application.streamingAssetsPath + "/GameSettings.json";

        string targetFile = System.IO.File.ReadAllText(fileLocation);

        GameSettings gameSettings = JsonUtility.FromJson<GameSettings>(targetFile);

	    foreach (RaceSettings race in gameSettings.Races)
	    {
            foreach (UnitSettings unit in race.Units)
            {
                string location = unit.FileLocation + "/" + unit.Name;
                GameObject prefab;
                try
                {
                    prefab = Resources.Load(location, typeof(GameObject)) as GameObject;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Debug.LogError("Exception: Unit name: " + unit.Name + " could not be loaded from: " + location);
                    continue;
                }

                if (prefab == null)
                {
                    Debug.LogError("Unit name: " + unit.Name + " could not be loaded from: " + location);
                    continue;
                }

                RaceProperty raceProperty = prefab.GetComponent<RaceProperty>();

                if (raceProperty == null)
                {
                    Debug.LogError("Unit name: " + unit.Name + " has no RaceProperty in location: " + location);
                    continue;
                }

                Race raceType = raceProperty.GetComponentValue();
                
                if (!Units.Keys.Contains(raceType))
                {
                    Units.Add(raceType, new List<GameObject>());
                }

                if (!Units[raceType].Contains(prefab))
                {
                    Units[raceType].Add(prefab);
                }
            }
        }
    }
}
