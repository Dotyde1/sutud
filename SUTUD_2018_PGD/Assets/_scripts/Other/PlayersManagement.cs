﻿using Sutud.Units;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sutud.Units.Behaviors;
using Sutud.Events;

public class PlayersManagement : MonoBehaviour
{

    #region singleton
    public static PlayersManagement instance;
    #endregion
    public enum GameStatus { activeWave, CompletedWave, awaitingNextWaveButton }
    public GameStatus gameStatus = GameStatus.awaitingNextWaveButton;

    public Text statusText;

    public List<PlayerManager> players = new List<PlayerManager>();
    // is to be moved to options later
    public bool snapCamera;
    public GameEvent startWave;
    public GameEvent endWave;

    public static int waveCounter = 0;

	[SerializeField]
	private GameObject winscreen;

	[SerializeField]
	private WinLoseTest winLoseText;

	public GameEvent GameOver;

    // Use this for initialization
    void Awake()
    {
        instance = this;
		waveCounter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (statusText != null)
            statusText.text = string.Format("{0}", gameStatus);

    }

    private void FixedUpdate()
    {
        switch (gameStatus)
        {
            case GameStatus.activeWave:
                int winnerID;
                if (HasFightEnded(out winnerID))
                {
                    foreach (PlayerManager player in players)
                    {
                        if (player.isAi)
                            player.income *= 1.005f;//AI cheat
                        player.gold += player.income;
                        player.gold -= player.maintenance;
                        if (player.playerID != winnerID)
                        {
                            player.gold += player.income * 0.2f;
                            player.lives--;
                            if (player.lives == 0)
                            {
                                Debug.Log(player.playerID + " Has lost the match");
                                WinnerScript.WriteWinner(winnerID);
								GameOver.Raise();
                            }
                        }
                        foreach (Unit unit in player.aliveUnits)
                        {
                            Destroy(unit.gameObject);
                        }
                    }
                    if (snapCamera)
                    {
                        endWave.Raise();
                    }
                    gameStatus = GameStatus.awaitingNextWaveButton;
                }
                break;
        }
    }

    public bool HasFightEnded(out int winner)
    {
        winner = -1;
        foreach (PlayerManager player in players)
        {
            if (player.aliveUnits.Count == 0)
            {
                if (player.playerID == 0)
                {
                    winner = 1;
                }
                else
                {
                    winner = 0;
                }
                return true;
            }
        }
        return false;
    }

    public PlayerManager FindPlayerByID(int id)
    {
        foreach (PlayerManager player in players)
            if (player.playerID == id)
                return player;
        return null;
    }

    public void ReadyButton()
    {
        if (gameStatus != GameStatus.awaitingNextWaveButton)
            return;

        startWave.Raise();
    }

    [ContextMenu("Send out wave")]
    public void StartWave()
    {
        if (gameStatus == GameStatus.awaitingNextWaveButton)
        {
            waveCounter++;
            foreach (PlayerManager player in players)
                player.myDropArea.SendWave();
            gameStatus = GameStatus.activeWave;
            startWave.Raise();
        }
    }




}
