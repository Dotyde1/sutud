﻿using Sutud.Units.Behaviors;
using Sutud.Units.Properties;
using UnityEngine;
using UnityEngine.UI;

public class ToolTipDisp : MonoBehaviour
{
    public LayerMask Mask;

    private Camera myCamera;

    public Text unitName, goldCost, damage, movementSpeed,
        range, attackRate, maxHealth, startingHealth, bounty;

    public GameObject toolTipPanel;
    // Use this for initialization
    void Start()
    {
        myCamera = GetComponent<Camera>();
        toolTipPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit, 200.0f, Mask))
        {
            toolTipPanel.SetActive(true);
            var unit = hit.collider.gameObject.GetComponent<Sutud.Units.Unit>();
            if(unit == null)
            {
                toolTipPanel.SetActive(false);
                return;
            }
            try
            {
                goldCost.text = unit.unitPropertiesManager.GetProperty<CostProperty>()._value.ToString();
            }
#pragma warning disable 0168
            catch(System.Exception e)
#pragma warning restore 0168
            {

            }
			unitName.text = unit.gameObject.name.Replace("(Clone)", "");
            var attackbehavior = unit.unitBehaviourManager.GetBehaviour<Melee>();
            if(attackbehavior == null)
            {
                var ranged = unit.unitBehaviourManager.GetBehaviour<RangedAttackBehavior>();
                if(ranged != null)
                {
                    damage.text = string.Format("{0} - {1}", ranged.minDamage, ranged.maxDamage);
                    range.text = ranged.range.ToString();
                    attackRate.text = ranged.RechargeTime.ToString();
                }
                else
                {
                    damage.text = "";
                    range.text = "";
                }
            }
            else
            {
                damage.text = string.Format("{0} - {1}", attackbehavior.minDamage, attackbehavior.maxDamage);
                range.text = attackbehavior.range.ToString();

                attackRate.text = attackbehavior.RechargeTime.ToString();
            }
            try
            {
                movementSpeed.text = unit.unitPropertiesManager.GetProperty<MovementSpeedProperty>()._value.ToString();
            }
#pragma warning disable 0168
            catch(System.Exception e)
#pragma warning restore 0168
            {

            }
            try
            {
                maxHealth.text = unit.unitPropertiesManager.GetProperty<HealthMaxProperty>()._value.ToString();
            }
#pragma warning disable 0168
            catch(System.Exception e)
#pragma warning restore 0168
            {

            }
            try
            {
                startingHealth.text = unit.unitPropertiesManager.GetProperty<HealthProperty>()._value.ToString();
            }
#pragma warning disable 0168
            catch(System.Exception e)
#pragma warning restore 0168
            {

            }
            try
            {
                bounty.text = unit.unitPropertiesManager.GetProperty<BountyProperty>()._value.ToString();
            }
#pragma warning disable 0168
            catch(System.Exception e)
#pragma warning restore 0168
            {

            }
        }
        else
        {
            if (toolTipPanel != null)
            toolTipPanel.SetActive(false);
        }

    }
}
