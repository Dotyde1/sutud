﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units;
/// <summary>
/// Manages a single players gold, lives and units(for now, atleast)
/// </summary>
public class PlayerManager : MonoBehaviour
{
    /// <summary>
    /// For tracking who this player is with an integer
    /// </summary>
    public int playerID;

    /// <summary>
    /// For tracking this players allied players
    /// </summary>
    public List<PlayerManager> alliances = new List<PlayerManager>();

    /// <summary>
    /// For tracking this players opponents
    /// </summary>
    public List<PlayerManager> opponents = new List<PlayerManager>();

    /// <summary>
    /// The color this player has, used to set decal colors
    /// </summary>
    public Color color = Color.black;

    /// <summary>
    /// For tracking this players alive units
    /// </summary>
    public List<Unit> aliveUnits = new List<Unit>();

    /// <summary>
    /// For tracking this players spawn units(the units on the build platform)
    /// </summary>
    public List<Unit> spawnUnits = new List<Unit>();

    /// <summary>
    /// Reference too this players drop area
    /// </summary>
    public DropAreaUnitsManager myDropArea;

    /// <summary>
    /// Defines weither or not this player is an AI or not
    /// </summary>
    public bool isAi = false;

    /// <summary>
    /// Defines the units found in the shop for this player
    /// Required for the AI!
    /// </summary>
    public List<GameObject> unitsInShop = new List<GameObject>();

    /// <summary>
    /// Players gold
    /// </summary>
    public float gold = 0;

    /// <summary>
    /// Players Income
    /// At the end of each wave this value is added too the players gold
    /// </summary>
    public float income = 100;

    /// Players Total amount of units 
    /// <summary> 
    public float numberofunits = 0;
    /// </summary> 

    /// <summary>
    /// Players Maintaence
    /// At the end of each wave this value is deducted from the players gold
    /// </summary>
    public float maintenance = 0;

    /// <summary>
    /// The players remaining lives
    /// </summary>
    public int lives = 10;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void InitializeAi(List<GameObject> unitsToPurchase)
    {

    }
}
