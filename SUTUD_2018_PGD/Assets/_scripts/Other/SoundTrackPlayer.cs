﻿using System.Collections;
using System.Collections.Generic;
using Sutud.Units.Weapons;
using Sutud.Units.Properties;
using UnityEngine;


namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(AudioSource))]

    public class SoundTrackPlayer : MonoBehaviour
    {
        
        public  AudioSource soundEmitter;

        public List<AudioClip> BattleSound;
        public List<AudioClip> BuildSound;


        public void PlayBattleSound()
        {
            if (soundEmitter.isPlaying)
            {
                soundEmitter.Stop();
            }
            soundEmitter.clip = BattleSound.GetRandomFromList();
            soundEmitter.Play();
        }

        public void PlayBuildSound()
        {
            if (soundEmitter.isPlaying)
            {
                soundEmitter.Stop();
            }
            soundEmitter.clip = BuildSound.GetRandomFromList();
            soundEmitter.Play();
        }

    }
}
