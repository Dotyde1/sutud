﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maximizeButton : MonoBehaviour {
	[SerializeField]
	private GameObject winScreen;
	[SerializeField]
	private GameObject minimizeWinScreen;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Maximize(){
		winScreen.SetActive(true);
		minimizeWinScreen.SetActive(false);
		Time.timeScale = 0f;
	}
}
