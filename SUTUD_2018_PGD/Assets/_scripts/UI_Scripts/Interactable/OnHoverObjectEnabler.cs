﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OnHoverObjectEnabler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool isOver = false;
    public List<GameObject> gameObjects;

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOver = true;
        foreach(var gameObject in gameObjects)
        {
            gameObject.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isOver = false;
        foreach (var gameObject in gameObjects)
        {
            gameObject.SetActive(false);
        }
    }
}
