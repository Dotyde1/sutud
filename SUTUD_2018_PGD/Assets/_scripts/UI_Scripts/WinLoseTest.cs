﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseTest : MonoBehaviour
{
    [SerializeField]
    private string[] loseTextQuotes = new string[] 
    {
        "Well... that sucked",
        "Ive seen worse but also better... much better",
        "Better luck next time... maybe",
        "Is it already over? Lucky me",
        "Loser"
    };
    [SerializeField]
    private Text text;
    [SerializeField]
    private GameObject winScreen;
    // Use this for initialization
    void Start()
    {
        if (winScreen != null)
        {
            winScreen.SetActive(false);
        }
        Time.timeScale = 1f;
    }

    public void GameOver()
    {
        Time.timeScale = 0f;
        winScreen.SetActive(true);
        string loseText = loseTextQuotes[Random.Range(0, loseTextQuotes.Length)];
        text.text = loseText;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            GameOver();
        }
    }
}
