﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimizeButton : MonoBehaviour {
	[SerializeField]
	private GameObject winScreen;
	[SerializeField]
	private GameObject minimizeWinScreen;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Minimize(){
		winScreen.SetActive(false);
		minimizeWinScreen.SetActive(true);
		Time.timeScale = 1f;
	}
}
