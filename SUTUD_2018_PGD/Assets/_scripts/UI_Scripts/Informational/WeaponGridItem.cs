﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using Sutud.Units.Weapons;

public class WeaponGridItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public WeaponsGrid GridDaddy;

    Weapon moniteringWeapon;

    public Image cooldownDisplay;
    public Image weaponArt;
    public Text damageRangeDisplay;

    public void OnPointerEnter(PointerEventData eventData)
    {
        GridDaddy.PointerEnter(moniteringWeapon);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GridDaddy.PointerExit();
    }

    public void AssignWeapon(Weapon weapon)
    {
        moniteringWeapon = weapon;
        weaponArt.sprite = weapon.weaponUiArt;
    }

    public void FixedUpdate()
    {
        cooldownDisplay.fillAmount = moniteringWeapon.cooldown / moniteringWeapon.attackSpeed;
        damageRangeDisplay.text = moniteringWeapon.damageRange.ToRoundString();
    }
}
