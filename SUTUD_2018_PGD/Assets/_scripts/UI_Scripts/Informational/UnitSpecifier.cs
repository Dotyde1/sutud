﻿using Sutud.Units;
using Sutud.Units.Properties;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitSpecifier : MonoBehaviour
{
    public HpBar healthBar;
    public ManaBar manaBar;
    public Text healthBarStatus;
    public Text manaBarStatus;
    public Text unitName;
    public Text unitStats;
    public Text unitToolTip;
    public Image unitUiIcon;
    private Sprite fallBackIcon;

    private Unit unitToWatch;
    private bool trackMana;
    private bool trackHealth;

    public void Start()
    {
        fallBackIcon = unitUiIcon.sprite;
        LoseUnit();
    }

    public void FixedUpdate()
    {
        if (trackMana)
            manaBarStatus.text = string.Format("{0}/{1}", manaBar.manaProp._value, manaBar.manaMaxProp._value);
        if(trackHealth)
            healthBarStatus.text = string.Format("{0}/{1}", healthBar.myHealthProp._value, healthBar.maxHpProp._value);

    }

    internal void ReceiveNewUnit(Unit unit)
    {
        this.ActivateHealthBar(unit);
        this.ActivateManaBar(unit);
        unitToolTip.text = string.Format("Gold cost: {0}\n" +
            "Bounty: {1}\n",
            unit.unitPropertiesManager.GetProperty<CostProperty>()._value, unit.unitPropertiesManager.GetProperty<BountyProperty>()._value);
        unitName.gameObject.SetActive(true);
        unitName.text = unit.name;
        unitStats.gameObject.SetActive(true);
        unitUiIcon.gameObject.SetActive(true);
        unitToWatch = unit;
        if (unit.UiIcon != null)
        {
            unitUiIcon.sprite = unit.UiIcon;
        }
        else
        {
            unitUiIcon.sprite = fallBackIcon;
        }
    }

    private void ActivateManaBar(Unit unit)
    {
        ManaProperty mana = unit.unitPropertiesManager.GetProperty<ManaProperty>();
        ManaMaxProperty manaMax = unit.unitPropertiesManager.GetProperty<ManaMaxProperty>();
        if (mana != null && manaMax != null)
        {
            trackMana = true;
            manaBar.gameObject.SetActive(true);
            manaBar.manaProp = mana;
            manaBar.manaMaxProp = manaMax;
        }
        else
        {
            manaBar.gameObject.SetActive(false);
        }
    }

    private void ActivateHealthBar(Unit unit)
    {
        HealthProperty health = unit.unitPropertiesManager.GetProperty<HealthProperty>();
        HealthMaxProperty healthMax = unit.unitPropertiesManager.GetProperty<HealthMaxProperty>();
        if (health != null && healthMax != null)
        {
            trackHealth = true;
            healthBar.gameObject.SetActive(true);
            healthBar.myHealthProp = health;
            healthBar.maxHpProp = healthMax;
        }
        else
        {
            healthBar.gameObject.SetActive(false);
        }
    }

    internal void LoseUnit()
    {
        Debug.Log("lost unit ");
        trackHealth = false;
        trackMana = false;
        unitToWatch = null;
        unitName.gameObject.SetActive(false);
        unitStats.gameObject.SetActive(false);
        unitUiIcon.gameObject.SetActive(false);
        healthBar.gameObject.SetActive(false);
        manaBar.gameObject.SetActive(false);
    }
}
