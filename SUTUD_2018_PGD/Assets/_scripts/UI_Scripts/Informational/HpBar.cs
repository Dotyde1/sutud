﻿using Sutud.Units;
using Sutud.Units.Properties;
using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    public bool onIfAtMaxHP = false;
    public Unit myUnit;
    public HealthProperty myHealthProp;
    public HealthMaxProperty maxHpProp;

    public Image GreenHealthbar;
    public Image RedHealthBar;

    private void Start()
    {
        myUnit = GetComponentInParent<Unit>();
    }

    // Update is called once per frame 
    void FixedUpdate()
    {
        if((myHealthProp == null || maxHpProp == null) && myUnit != null)
        {
            if(myHealthProp == null)
            {
                myHealthProp = myUnit.unitPropertiesManager.GetProperty<HealthProperty>();
            }
            if(maxHpProp == null)
            {
                maxHpProp = myUnit.unitPropertiesManager.GetProperty<HealthMaxProperty>();
            }
        }
        else
        {
            if(maxHpProp._value != myHealthProp._value)
            {
                GreenHealthbar.fillAmount = myHealthProp._value / maxHpProp._value;
                RedHealthBar.fillAmount = 1;
            }
            else
            {
                if (onIfAtMaxHP)
                {
                    GreenHealthbar.fillAmount = 1;
                    RedHealthBar.fillAmount = 1;
                }
                else
                {
                    GreenHealthbar.fillAmount = 0;
                    RedHealthBar.fillAmount = 0;
                }
            }
        }
    }
}