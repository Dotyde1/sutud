﻿using Sutud.Units;
using Sutud.Units.Behaviors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomSectionManager : MonoBehaviour
{
    public WeaponsGrid weaponGrid;
    public SpellsAndEffectsGrid spellGrid;
    public UnitSpecifier unitUi;

    public void Awake()
    {
        LoseUnitInterest();
    }

    public void OnNewUnit(Unit unit)
    {
        LoseUnitInterest();
        unitUi.ReceiveNewUnit(unit);
        spellGrid.PassHealingBehavior(unit.unitBehaviourManager.GetBehaviour<HealingBehaviour>());
        var useWeaponsBehavior = unit.unitBehaviourManager.GetBehaviour<UseWeaponsBehavior>();
        if (useWeaponsBehavior != null)
            weaponGrid.ReceiveNewWeapons(useWeaponsBehavior.Weapons);
        else
            weaponGrid.ClearWeapons();
    }

    public void LoseUnitInterest()
    {
        weaponGrid.ClearWeapons();
        unitUi.LoseUnit();
    }
}
