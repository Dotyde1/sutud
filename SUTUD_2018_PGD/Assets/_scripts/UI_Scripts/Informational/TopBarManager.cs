﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopBarManager : MonoBehaviour {

    public Text UnitInfo, Gold, Lives;
    public int playerID;
    public PlayerManager player;

    public void Start()
    {
        GetPlayerByID(playerID);
    }

    public void FixedUpdate()
    {
        if (player != null)
        {
            UnitInfo.text = string.Format("{0}/{1}", player.aliveUnits.Count, player.spawnUnits.Count);
            Gold.text = string.Format("{0}", player.gold);
            Lives.text = string.Format("{0}", player.lives);
        }
    }
    //Temp method to toggle
    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            playerID++;
            if (playerID >= PlayersManagement.instance.players.Count)
            {
                playerID = 0;
            }
            GetPlayerByID(playerID);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            playerID--;
            if(playerID < 0)
            {
                playerID = PlayersManagement.instance.players.Count - 1;
            }
            GetPlayerByID(playerID);
        }
    }

    private void GetPlayerByID(int playerID)
    {
        player = PlayersManagement.instance.FindPlayerByID(playerID);
    }
}
