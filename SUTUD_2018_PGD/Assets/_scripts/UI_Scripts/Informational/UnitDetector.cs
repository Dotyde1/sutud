﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units;

public class UnitDetector : MonoBehaviour
{

    public LayerMask Mask;
    public BottomSectionManager BottomUiSectionManager;
    public Unit trackingUnit;
    public Unit trackingUnitPrev;

    private Camera myCamera;

    // Use this for initialization
    void Start()
    {
        myCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    /// <summary>
    /// Updates this instance.
    /// </summary>
    void Update()
    {
        if(!trackingUnit && trackingUnitPrev != null)
        {
            trackingUnit = null;
            Debug.Log("This never happens I guess?");
            BottomUiSectionManager.LoseUnitInterest();
        }
        trackingUnitPrev = trackingUnit;
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 200.0f, Mask))
            {
                var unit = hit.collider.gameObject.GetComponent<Unit>();
                if (unit != null)
                {
                    if (unit != trackingUnit)
                        BottomUiSectionManager.OnNewUnit(unit);
                    trackingUnit = unit;
                }
                else
                {
                    BottomUiSectionManager.LoseUnitInterest();
                }
            }
            else
            {
                BottomUiSectionManager.LoseUnitInterest();
            }
        }
    }
}

