﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sutud.Units.Weapons;
using UnityEngine;
using System.Text;
using UnityEngine.UI;

public class WeaponsGrid : MonoBehaviour {

    public WeaponGridItem[] weaponGridItems;
    public GameObject TooltipBox;
    public Text tooltipText;
    private Weapon weapon;

    public void Start()
    {
        ClearWeapons();
    }

    public void ReceiveNewWeapons(Weapon[] unitWeapons)
    {
        for(int i = 0; i < unitWeapons.Length; i++)
        {
            if(i >= weaponGridItems.Length)
            {
                Debug.LogWarning("Maximum weapons hit. UI cannot handle more weapons, fix this?");
                break;
            }
            weaponGridItems[i].gameObject.SetActive(true);
            weaponGridItems[i].AssignWeapon(unitWeapons[i]);
        }
    }

    public void ClearWeapons()
    {
        foreach(var weaponGridItem in weaponGridItems)
        {
            weaponGridItem.gameObject.SetActive(false);
        }
        weapon = null;
        TooltipBox.SetActive(false);
    }

    internal void PointerEnter(Weapon moniteringWeapon)
    {
        weapon = moniteringWeapon;
        TooltipBox.SetActive(true);
    }

    internal void PointerExit()
    {
        weapon = null;
        TooltipBox.SetActive(false);
    }

    public void FixedUpdate()
    {
        if (TooltipBox.activeInHierarchy)
            UpdateTooltipString();
    }

    public void UpdateTooltipString()
    {
        StringBuilder tooltipString = new StringBuilder();
        tooltipString.AppendFormat("Damage: {0}\n" +
            "Attackspeed: {1}\n" +
            "AttackRange: {2}\n",
            weapon.damageRange.ToRoundString(),
            weapon.attackSpeed,
            weapon.weaponRange);
        if (weapon.minimumRange != 0)
            tooltipString.AppendFormat("Minimum range: {0}\n", weapon.minimumRange);
        tooltipString.AppendFormat("Is ranged: {0}\n", weapon.isRanged);
        if (weapon.dealsAoeDamage)
        {
            tooltipString.AppendFormat("AoE range: {0}\n", weapon.AoeRange);
            tooltipString.AppendFormat("AoE damage multiplier: {0}\n", weapon.AoeDamageMultiplier);
        }
        if(weapon.critChance != 0)
        {
            tooltipString.AppendFormat("Crit Chance: {0}\n" +
                "Crit multiplier: {1}", weapon.critChance, weapon.critDamageMultiplier);
        }
        if (!String.IsNullOrEmpty(weapon.weaponFlavorText))
        {
            tooltipString.AppendFormat("/n{0}", weapon.weaponFlavorText);
        }
        tooltipText.text = tooltipString.ToString();
    }
}
