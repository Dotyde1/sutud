﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpFaceCamera : MonoBehaviour {


    public Camera m_Camera;

    void Awake()
    {
        if (m_Camera == null)
        {
            m_Camera = Camera.main;
        }
    }


        // Update is called once per frame
        void Update () {

        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
            m_Camera.transform.rotation * Vector3.up);
    }
}
