﻿using Sutud.Units;
using Sutud.Units.Properties;
using UnityEngine;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{
    public bool onIfAtMaxMana = false;
    public Unit myUnit;

    public ManaProperty manaProp;
    public ManaMaxProperty manaMaxProp;

    public Image DarkBlueHealthbar;
    public Image LightBlueHealthBar;

    private void Start()
    {
        myUnit = GetComponentInParent<Unit>();
    }

    // Update is called once per frame 
    void FixedUpdate()
    {
        if(manaProp == null || manaMaxProp == null)
        {
            if(manaProp == null)
            {
                manaProp = myUnit.unitPropertiesManager.GetProperty<ManaProperty>();
            }
            if(manaMaxProp == null)
            {
                manaMaxProp = myUnit.unitPropertiesManager.GetProperty<ManaMaxProperty>();
            }
        }
        else
        {
            if(manaMaxProp._value != manaProp._value)
            {
				DarkBlueHealthbar.fillAmount = manaProp._value / manaMaxProp._value;
				LightBlueHealthBar.fillAmount = 100;
            }
            else
            {
                if (onIfAtMaxMana)
                {
                    DarkBlueHealthbar.fillAmount = 1;
                    LightBlueHealthBar.fillAmount = 1;
                }
                else
                {
                    DarkBlueHealthbar.fillAmount = 0;
                    LightBlueHealthBar.fillAmount = 0;
                }
            }
        }
    }
}