﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinnerScript : MonoBehaviour
{
    static public Text TextShit;
    public Text TextShit2;
    public PlayersManagement playersManagement;

    private static Highscore highscore;

    // Use this for initialization
    void Awake()
    {
        WinnerScript.TextShit = TextShit2;
    }

    void Start()
    {
        WinnerScript.TextShit = TextShit2;
    }

    // Update is called once per frame
    void Update()
    {
        if (WinnerScript.TextShit == null)
        {
            WinnerScript.TextShit = TextShit2;
        }
    }

    public static void WriteWinner(int winner)
    {
        if (highscore == null)
        {
            highscore = new Highscore();
        }
    
        // todo: add name and score
        highscore.AddHighscore(PlayerNameStorer.GetPlayerName(), PlayersManagement.waveCounter);

		WinnerScript.TextShit.text = PlayerNameStorer.GetPlayerName() + " Survived " + PlayersManagement.waveCounter + " waves";

    }
}
