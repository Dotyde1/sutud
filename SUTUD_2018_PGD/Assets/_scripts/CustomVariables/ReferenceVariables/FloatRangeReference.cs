﻿using System;

namespace Sutud.Variables.ReferenceVariables
{
    [Serializable]
    public class FloatRangeReference : AbstractVariableReference<FloatRangeVariable, FloatRange>
    {

    }
}
