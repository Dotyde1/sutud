﻿using UnityEngine;

namespace Sutud.Variables.RuntimeSets
{
    [CreateAssetMenu(fileName ="NewGameobjectRuntimeSet", menuName ="RuntimeSets/GameObjectRuntimeSet")]
    public class GameObjectRuntimeSet : AbstractRuntimeSet<GameObject>
    {

    }
}

