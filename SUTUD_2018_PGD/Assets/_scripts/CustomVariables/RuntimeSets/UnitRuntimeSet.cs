﻿using Sutud.Units;
using UnityEngine;

namespace Sutud.Variables.RuntimeSets
{
    [CreateAssetMenu(fileName = "NewUnitRuntimeSet", menuName = "RuntimeSets/UnitRuntimeSet")]
    public class UnitRuntimeSet : AbstractRuntimeSet<Unit>
    {

    }
}