﻿using Sutud.Cameras;
using UnityEngine;
namespace Sutud.Variables.RuntimeSets
{
    [CreateAssetMenu(fileName = "NewCameraPointRunTimeSet", menuName = "RuntimeSets/CameraPointRunTimeSet")]
    public class CameraPointRunTimeSet : AbstractRuntimeSet<CameraPoint>
    {

    }
}