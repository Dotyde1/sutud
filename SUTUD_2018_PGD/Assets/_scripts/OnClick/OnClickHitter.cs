﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class OnClickHitter : MonoBehaviour
{

    Camera myCamera;


    // Use this for initialization
    void Start()
    {
        myCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                var hitable = hit.transform.gameObject.GetComponent<IHitable>();
                if (hitable != null)
                    hitable.Hit(this.gameObject);
                Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object
            }
        }
    }
}
