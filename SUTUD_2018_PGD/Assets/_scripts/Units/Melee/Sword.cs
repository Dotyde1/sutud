﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour {
	public float swingingSpeed = 2f;
	public float returnSpeed = 2f;
	public float returnDuration = 0.5f;
	public float attackDuration = 0.35f;

	private Quaternion targetRotation;

	private bool isAttacking;

	private float cooldownTimer;
	// Use this for initialization
	void Start () {
		targetRotation = Quaternion.Euler (0, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (isAttacking) {
			transform.localRotation = Quaternion.Lerp (transform.localRotation, targetRotation, Time.deltaTime * swingingSpeed);
		} else {
			transform.localRotation = Quaternion.Lerp (transform.localRotation, targetRotation, Time.deltaTime * (isAttacking ? swingingSpeed : returnSpeed));
		}
		cooldownTimer -= Time.deltaTime;
	}

    [ContextMenu("Attack")]
	public void Attack(){
		if (cooldownTimer > 0f) {
			return;
		}

		targetRotation = Quaternion.Euler (90, 0, 0);

		cooldownTimer = returnDuration;

		StartCoroutine (CooldownWait ());
	}

	private IEnumerator CooldownWait(){
		isAttacking = true;

		yield return new WaitForSeconds (attackDuration);

		isAttacking = false;

		targetRotation = Quaternion.Euler (0, 0, 0);
	}
}
