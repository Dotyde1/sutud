﻿using System;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors.TargetingBehaviors
{
    [CreateAssetMenu(fileName = "NewDistanceTargetingBehavior", menuName = "Targeting Behaviors/Distance Targeting Behavior")]
    public class DistanceTargetingBehavior : AbstractUnitTargetingBehavior
    {
        private float divisor = 5000;
        private float edge = 4;
        private float inEdgeDecline = 100;
        public override float InternalCalculateHeuristic(Unit self, Unit unit)
        {
            float distance = (self.transform.position - unit.transform.position).sqrMagnitude;
            if (distance > edge)
                return divisor / distance;
            return divisor - distance * inEdgeDecline;
        }
    }
}