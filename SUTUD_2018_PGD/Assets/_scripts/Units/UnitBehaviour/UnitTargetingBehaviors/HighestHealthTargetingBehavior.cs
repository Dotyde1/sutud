﻿using System;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors.TargetingBehaviors
{
    [CreateAssetMenu(fileName = "NewHighestHealthTargetingBehavior", menuName = "Targeting Behaviors/Highest Health Targeting Behavior")]
    public class HighestHealthTargetingBehavior : AbstractUnitTargetingBehavior
    {
        public override float InternalCalculateHeuristic(Unit self, Unit unit)
        {
            return unit.unitPropertiesManager.GetProperty<HealthProperty>()._value;
        }
    }
}