﻿using Sutud.Units.Properties;
using UnityEngine;
namespace Sutud.Units.Behaviors.TargetingBehaviors
{
    [CreateAssetMenu(fileName = "NewMissingHealthTargetingBehavior", menuName = "Targeting Behaviors/Missing Health Targeting Behavior")]
    public class MissingHealthTargetingBehavior : AbstractUnitTargetingBehavior
    {
        public override float InternalCalculateHeuristic(Unit self, Unit unit)
        {
            var hpProp = unit.unitPropertiesManager.GetProperty<HealthProperty>();
            var maxHpProp = unit.unitPropertiesManager.GetProperty<HealthMaxProperty>();
            return maxHpProp._value - hpProp._value;
        }
    }
}
