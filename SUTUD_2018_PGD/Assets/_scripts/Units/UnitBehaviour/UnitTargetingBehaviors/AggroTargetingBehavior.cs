﻿using System;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors.TargetingBehaviors
{
    [CreateAssetMenu(fileName = "NewAggroTargetingBehavior", menuName = "Targeting Behaviors/Aggro Targeting Behavior")]
    public class AggroTargetingBehavior : AbstractUnitTargetingBehavior
    {
        public override float InternalCalculateHeuristic(Unit self, Unit unit)
        {
            return unit.unitPropertiesManager.GetProperty<AggroProperty>()._value;
        }
    }
}