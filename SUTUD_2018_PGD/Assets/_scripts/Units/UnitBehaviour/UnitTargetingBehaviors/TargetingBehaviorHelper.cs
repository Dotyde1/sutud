﻿using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors.TargetingBehaviors
{
    public enum FriendOrFoeTargeting { None, Friend, Foe, Both }
    public static class TargetingBehaviorHelper
    {
        public static bool DoesUnitMatchWithTargetingBehaviorMode(FriendOrFoeTargeting behaviorMode, FriendOrFoeTargeting unitFriendOrFoe)
        {
            switch (behaviorMode)
            {
                case FriendOrFoeTargeting.Both:
                    return unitFriendOrFoe == FriendOrFoeTargeting.Both
                        || unitFriendOrFoe == FriendOrFoeTargeting.Foe
                        || unitFriendOrFoe == FriendOrFoeTargeting.Friend;

                case FriendOrFoeTargeting.Foe://intended fall through
                case FriendOrFoeTargeting.Friend:
                    return unitFriendOrFoe == behaviorMode;
                case FriendOrFoeTargeting.None:
                    return false;
                default:
                    Debug.LogWarningFormat("Unit target match error, could not find {0} in the DoesUnitMatchWithTargetingBehaviorMode method", behaviorMode);
                    return false;
            }
        }
        //TODO: If we ever add 2v2 or shit. This NEEDS to be changed as it doesn't take into account if 2 units are in an alliance
        public static FriendOrFoeTargeting GetFriendOrFoeOfUnitRelativeToUnit(Unit relativeToUnit, Unit unitToGetFriendOrFoeFrom)
        {
            if (relativeToUnit.unitPropertiesManager.GetProperty<OwnerIDProperty>()._value == unitToGetFriendOrFoeFrom.unitPropertiesManager.GetProperty<OwnerIDProperty>()._value)
                return FriendOrFoeTargeting.Friend;
            else
            {
                return FriendOrFoeTargeting.Foe;
            }
        }

        public static FriendOrFoeTargeting GetFriendOrFoeOfUnitRelativeToPlayer(PlayerManager relativeToPlayer, Unit unitToGetFriendOrFoeFrom)
        {
            if (relativeToPlayer == null)//units that don't have an owner
            {
                if (unitToGetFriendOrFoeFrom.myPlayer == null)//are friends with other units that don't have owner
                    return FriendOrFoeTargeting.Friend;
                return FriendOrFoeTargeting.Foe;//and enemies with everything else
            }

            //Most of the time we target enemies
            foreach (PlayerManager player in relativeToPlayer.opponents)
            {
                if (player.aliveUnits.Contains(unitToGetFriendOrFoeFrom))
                {
                    return FriendOrFoeTargeting.Foe;
                }
            }
            //if we are owned by the same player we are friends
            if (relativeToPlayer == unitToGetFriendOrFoeFrom.myPlayer)
            {
                return FriendOrFoeTargeting.Friend;
            }

            //If we ever get to a 2v2 mode, this shit wil already just, work so yay
            foreach (PlayerManager player in relativeToPlayer.alliances)
            {
                if (player.aliveUnits.Contains(unitToGetFriendOrFoeFrom))
                {
                    return FriendOrFoeTargeting.Friend;
                }
            }
            return FriendOrFoeTargeting.None;
        }
    }
}