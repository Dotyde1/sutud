﻿using Sutud.Units.Properties;
using System.Collections.Generic;
using UnityEngine;

namespace Sutud.Units.Behaviors.TargetingBehaviors
{
    abstract public class AbstractUnitTargetingBehavior : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        public string developerTooltip;
        public bool debug = false;
#endif

        [Tooltip("If this targetingbehavior should handle friendlies, foes or both")]
        public FriendOrFoeTargeting friendOrFoeTargeting = FriendOrFoeTargeting.Foe;

        [Tooltip("Result of the heuristic is multiplied by this value")]
        [Range(1, 100)]
        public float importanceOfThisTargetingBehavior = 1;

        [Tooltip("Classifications this targetingbehavior should ignore")]
        public List<UnitClassifications> targetingClassificationBlackList = new List<UnitClassifications>();

        [Tooltip("Classifications this targetingbehavior can target. If left blanc can target all classifications(excluding those included in the blackList)")]
        public List<UnitClassifications> targetingClassificationWhiteList = new List<UnitClassifications>();

        [Tooltip("If this targeting behavior should favor certain unit classifications add them here(along with how much")]
        public List<ClassificationEntry> priorityList = new List<ClassificationEntry>();

        /// <summary>
        /// Handles the unit list.
        /// </summary>
        /// <param name="units">The units.</param>
        /// <returns></returns>
        public Dictionary<Unit, float> HandleUnitList(Unit self, List<Unit> units)
        {
            Dictionary<Unit, float> result = new Dictionary<Unit, float>();
            foreach (Unit unit in units)
            {
                result.Add(unit, CalculateHeuristic(self, unit));
            }
            return result;
        }

        /// <summary>
        /// Handles the unit list safely.
        /// </summary>
        /// <param name="units">The units.</param>
        /// <returns>A dictionary containing each unit and the result of it's heuristic</returns>
        public Dictionary<Unit, float> HandleUnitListSafe(Unit self, List<Unit> units)
        {
            Dictionary<Unit, float> result = new Dictionary<Unit, float>();
            foreach (Unit unit in units)
            {
                result.Add(unit, CalculateHeuristicSafe(self, unit));
            }
            return result;
        }

        /// <summary>
        /// Calculates the heuristic safely.
        /// </summary>
        /// <param name="unit">The unit.</param>
        /// <returns>Returns -1 if the classification is included in the blacklist,
        /// or not included in the whitelist</returns>
        public float CalculateHeuristicSafe(Unit self, Unit unit)
        {
            return CalculateHeuristicSafe(self, unit, unit.unitPropertiesManager.GetProperties<ClassificationProperty>());
        }

        /// <summary>
        /// Calculates the heuristic safely with the classifications precalculated.
        /// </summary>
        /// <param name="self">A reference to this unit</param>
        /// <param name="unit">The unit.</param>
        /// <param name="unitFoundClassifications">The classifications if the unit.</param>
        /// <returns>The heuristic OR 0 if the </returns>
        public float CalculateHeuristicSafe(Unit self, Unit unit, ClassificationProperty[] unitFoundClassifications)
        {
            if (targetingClassificationWhiteList.Count == 0)
            {
                if (targetingClassificationBlackList.Count == 0)
                    return CalculateHeuristic(self, unit);

                foreach (ClassificationProperty classificationProperty in unitFoundClassifications)
                {
                    if (targetingClassificationBlackList.Contains(classificationProperty._value.classification))
                        return 0;
                }
                return CalculateHeuristic(self, unit);
            }

            foreach (ClassificationProperty classificationProperty in unitFoundClassifications)
            {
                if (targetingClassificationBlackList.Contains(classificationProperty._value.classification))
                    return 0;
                if (!targetingClassificationWhiteList.Contains(classificationProperty._value.classification))
                    return 0;
            }

            return CalculateHeuristic(self, unit);
        }
        /// <summary>
        /// Calculates the heuristic.
        /// </summary>
        /// <param name="unit">The unit.</param>
        /// <returns>Returns the calculated heuristic, always above 0 and never larger then the maximum float value</returns>
        public float CalculateHeuristic(Unit self, Unit unit)
        {
#if UNITY_EDITOR
            float returnvalue = Mathf.Clamp(InternalCalculateHeuristic(self, unit) * importanceOfThisTargetingBehavior, float.Epsilon, float.MaxValue);
            if (debug)
            {
                Debug.Log("Return value of heuristic was " + returnvalue);
            }
            return returnvalue;
#else
            return Mathf.Clamp(InternalCalculateHeuristic(self, unit) * importanceOfThisTargetingBehavior, float.Epsilon, float.MaxValue);
#endif
        }

        /// <summary>
        /// Calculates the heuristic. Implemented method. 
        /// MUST NEVER CREATE A HEURISTIC THAT RETURNS SMALLER OR EQUALS TOO 0
        /// </summary>
        /// <param name="unit">The unit.</param>
        /// <returns>Returns the calculated heuristic</returns>
        public abstract float InternalCalculateHeuristic(Unit self, Unit unit);
    }
}