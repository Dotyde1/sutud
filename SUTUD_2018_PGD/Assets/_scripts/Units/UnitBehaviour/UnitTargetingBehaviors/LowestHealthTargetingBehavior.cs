﻿using Sutud.Units.Properties;
using UnityEngine;
namespace Sutud.Units.Behaviors.TargetingBehaviors
{
    [CreateAssetMenu(fileName = "NewLowestHealthTargetingBehavior", menuName = "Targeting Behaviors/Lowest health targeting behavior")]
    public class LowestHealthTargetingBehavior : AbstractUnitTargetingBehavior
    {
        private float divisor = 5000;
        private float edge = 4;
        private float inEdgeDecline = 100;
        public override float InternalCalculateHeuristic(Unit self, Unit unit)
        {
            var hpProperty = unit.unitPropertiesManager.GetProperty<HealthProperty>();
            if(hpProperty._value > edge)
                return divisor / hpProperty._value;
            return divisor - hpProperty._value * inEdgeDecline;
        }
    }
}
