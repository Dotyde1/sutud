﻿using Sutud.Units.Behaviors.TargetingBehaviors;
using Sutud.Units.Properties;
using Sutud.Variables.ReferenceVariables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(SightDistanceProperty))]
    public class UnitTargetingBehaviorManager : UnitBehaviour
    {

        [Tooltip("If this targetingBehaviorMananger should handle friendlies, foes or both")]
        public FriendOrFoeTargeting friendOrFoeTargeting = FriendOrFoeTargeting.Both;

        [Tooltip("Classifications this targetingBehaviorMananger should ignore")]
        [HideInInspector]//as this isn't implemented as of yet I've opted to hide the option in the inspector
        public List<UnitClassifications> targetingClassificationBlackList = new List<UnitClassifications>();

        [Tooltip("Classifications this targetingBehaviorMananger can target. If left blanc can target all classifications(excluding those included in the blackList)")]
        [HideInInspector]//as this isn't implemented as of yet I've opted to hide the option in the inspector
        public List<UnitClassifications> targetingClassificationWhiteList = new List<UnitClassifications>();

        [Tooltip("How likely it is for this targetBehaviorManager to swap to a new target." +
            "Will swap to a new target if that target evals with a value that is higher then the newest higest / stayingpower")]
        [Range(0.5f, 4)]
        public float stayingPower = 1.5f;

        [Tooltip("How frequently this targetingBehaviorManagerUpdates everything")]
        [MinMaxRange(0, 5f)]
        [SerializeField]
        FloatRangeReference updateTimeRange;

        public float nextUpdateIn = 1;

        [SerializeField]
        [HideInInspector]
        SightDistanceProperty sightDistance;

        [SerializeField]
        List<AbstractUnitTargetingBehavior> myTargetingBehaviors = new List<AbstractUnitTargetingBehavior>();

        List<Unit> detectedUnits = new List<Unit>();

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            sightDistance = unit.unitPropertiesManager.GetProperty<SightDistanceProperty>();
            base.Bake(unit);
        }

        public override void UnBake()
        {
            sightDistance = null;
            base.UnBake();
        }
#endif

		public override void Initialize()
        {
			base.Initialize();
            this.nextUpdateIn = updateTimeRange.Value.GetRandomValue();
        }

        public void RunFullTargetingBehavior()
        {
            Properties.KeyValuePair<Unit, float> newTarget = ResolveAllTargetingBehaviorsForUnitsList(UpdateDetectedUnitsList());

            if (unit.target == null)
            {
                unit.SetTargetUnit(newTarget.Key);
                return;
            }

            if (this.CalculateSingleUnitOverAllTargetingBehaviors(this.unit.target) * stayingPower < newTarget.Value)
            {
                unit.SetTargetUnit(newTarget.Key);
                return;
            }

        }

        //TODO: Add in black and whitelists(If we want this feature atleast)
        public List<Unit> UpdateDetectedUnitsList()
        {
            if (unit.myPlayer == null)
            {
                //The problem here is not detecting units owned by players, the problem is detecting units NOT owned by players(currently they have no listing anywhere)
                Debug.LogWarning("MyPlayer is null can't evaluate units detected, please extend to enable to functionality OR resolve the bug");
                return null;
            }

            List<Unit> unitsDetected = new List<Unit>();
            if (this.friendOrFoeTargeting == FriendOrFoeTargeting.Foe || this.friendOrFoeTargeting == FriendOrFoeTargeting.Both)
                foreach (PlayerManager opponent in unit.myPlayer.opponents)
                    foreach (Unit opponentUnit in opponent.aliveUnits)
                        if (IsUnitInSightRange(opponentUnit))
                            unitsDetected.Add(opponentUnit);

            if (this.friendOrFoeTargeting == FriendOrFoeTargeting.Friend || this.friendOrFoeTargeting == FriendOrFoeTargeting.Both)
            {
                //see whats in our allies
                foreach (PlayerManager ally in unit.myPlayer.alliances)
                    foreach (Unit allyUnit in ally.aliveUnits)
                        if (IsUnitInSightRange(allyUnit))
                            unitsDetected.Add(allyUnit);
                //see what we got
                foreach (Unit myUnit in unit.myPlayer.aliveUnits)
                {
                    if (myUnit == this.unit)
                        continue;

                    if (IsUnitInSightRange(myUnit))
                        unitsDetected.Add(myUnit);
                }
            }
            this.detectedUnits = unitsDetected;
            return unitsDetected;
        }

        public bool IsUnitInSightRange(Unit unitToTestSightRangeWith)
        {
            return ((unitToTestSightRangeWith.transform.position - this.transform.position).sqrMagnitude < sightDistance._value);
        }

        public Properties.KeyValuePair<Unit, float> ResolveAllTargetingBehaviorsForUnitsList(List<Unit> units)
        {
            Properties.KeyValuePair<Unit, float> result = new Properties.KeyValuePair<Unit, float>()
            {
                Key = null,
                Value = 0
            };
            foreach (Unit unitToCalc in units)
            {
                float currentUnitResult = CalculateSingleUnitOverAllTargetingBehaviors(unitToCalc);
                if (currentUnitResult > result.Value)
                {
                    result.Key = unitToCalc;
                    result.Value = currentUnitResult;
                }
            }
            return result;
        }

        public float CalculateSingleUnitOverAllTargetingBehaviors(Unit unitToCalc)
        {
            float result = 0;
            int behaviorsWithNoneZeroResults = 0;
            ClassificationProperty[] unitFoundClassifications = unitToCalc.unitPropertiesManager.GetProperties<ClassificationProperty>();
            FriendOrFoeTargeting unitFoF = TargetingBehaviorHelper.GetFriendOrFoeOfUnitRelativeToUnit(this.unit, unitToCalc);
            for (int i = 0; i < myTargetingBehaviors.Count; i++)
            {
                //only handle a behavior if it matches with the behaviors FoF settings
                if (TargetingBehaviorHelper.DoesUnitMatchWithTargetingBehaviorMode(myTargetingBehaviors[i].friendOrFoeTargeting, unitFoF))
                {
                    float behaviorResult = 0;
                    behaviorResult = CalculateSingleUnitOverTargetingBehavior(unitToCalc, myTargetingBehaviors[i], unitFoundClassifications);
                    if (behaviorResult != 0)
                        behaviorsWithNoneZeroResults++;
                    result += behaviorResult;
                }
            }
            //divide all results by the number of handled behaviors to normalize the results
            if (behaviorsWithNoneZeroResults != 0)
                result /= behaviorsWithNoneZeroResults;
            return result;
        }

        public float CalculateSingleUnitOverTargetingBehavior(Unit unitToCalc, AbstractUnitTargetingBehavior behaviorToCalcOver)
        {
            return behaviorToCalcOver.CalculateHeuristicSafe(this.unit, unitToCalc, unit.unitPropertiesManager.GetProperties<ClassificationProperty>());
        }

        public float CalculateSingleUnitOverTargetingBehavior(Unit unitToCalc, AbstractUnitTargetingBehavior behaviorToCalcOver, ClassificationProperty[] unitFoundClassifications)
        {
            return behaviorToCalcOver.CalculateHeuristicSafe(this.unit, unitToCalc, unitFoundClassifications);
        }

        public override void CustomFixedUpdate()
        {
            this.nextUpdateIn -= Time.fixedDeltaTime;
            if (this.nextUpdateIn < 0)
            {
                this.nextUpdateIn = updateTimeRange.Value.GetRandomValue();
                this.RunFullTargetingBehavior();
            }
        }

        public override void CustomOnDestroy()
        {
        }

        public override void CustomUpdate()
        {
        }

        private void OnDrawGizmosSelected()
        {
            foreach (Unit unit in detectedUnits)
            {
                if (unit == null)
                    continue;

                if (unit == this.unit.target)
                    Gizmos.color = Color.blue;
                else
                {
                    if (TargetingBehaviorHelper.GetFriendOrFoeOfUnitRelativeToUnit(this.unit, unit) == FriendOrFoeTargeting.Friend)
                    {
                        Gizmos.color = Color.green;
                    }
                    else
                    {
                        Gizmos.color = Color.red;
                    }
                }
                Gizmos.DrawLine(this.transform.position, unit.transform.position);
            }
        }
    }
}
