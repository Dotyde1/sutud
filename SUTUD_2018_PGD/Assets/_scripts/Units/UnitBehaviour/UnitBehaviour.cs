﻿using System.Collections;
using System.Collections.Generic;
using System;
using Sutud.Units.Properties;
using Sutud.Units.UnitStates;
using UnityEngine;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(UnitBehaviourManager))]
    public abstract class UnitBehaviour : MonoBehaviour
    {
        [Tooltip("In what UnitStates to be active.")]
        public List<UnitState> activeUnitStates = new List<UnitState>();

        internal UnitBehaviourManager unitBehaviourManager;
        [HideInInspector]
        [SerializeField]
        internal Unit unit;
        [HideInInspector]
        [SerializeField]
        internal UnitStateProperty unitStateProperty;

        //implement a monobehavior method to display enabled/disabled checkmark in inspector :)
        private void Start()
        {

        }

		public virtual void Initialize()
        {
            unit.OnStateChange += OnStateChangeListener;
        }

		public virtual void Initialize(Unit unit)
        {
            this.unit = unit;
            unit.OnStateChange += OnStateChangeListener;
        }

        public abstract void CustomUpdate();

        public abstract void CustomFixedUpdate();

        public abstract void CustomOnDestroy();

        public virtual void OnStateChangeListener(object sender, Unit.UnitStateEventArg eventArg)
        {
            if (unitStateProperty == null)
            {
                Debug.LogWarningFormat("Unit was not baked properly! Rebake {0}! Could not find {1}", this.unit.name, typeof(UnitStateProperty));
            }

            this.enabled = this.activeUnitStates.Contains(eventArg.state);
        }

#if UNITY_EDITOR
        public virtual void Bake(Unit unit)
        {
            this.unit = unit;
            this.unitStateProperty = unit.unitPropertiesManager.GetProperty<UnitStateProperty>();
            this.enabled = this.activeUnitStates.Contains(unitStateProperty._value);
        }

        public virtual void UnBake()
        {
            this.unit = null;
            this.unitStateProperty = null;
            this.enabled = true;
        }
#endif
    }
}
