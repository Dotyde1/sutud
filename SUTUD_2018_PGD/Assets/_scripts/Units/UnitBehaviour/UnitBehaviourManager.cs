﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(Unit))]
    public class UnitBehaviourManager : MonoBehaviour
    {
        [SerializeField]
        private List<UnitBehaviour> unitBehaviours;
        internal Unit unit;

        private void Start()
        {

        }

		public void Initialize()
        {
            foreach (UnitBehaviour unitBehaviour in this.unitBehaviours)
            {
				unitBehaviour.Initialize();
            }
        }
        #region bake
#if UNITY_EDITOR
        public void Bake(Unit unit)
        {
            this.unit = unit;
            enabled = true;
            unitBehaviours = new List<UnitBehaviour>(GetComponents<UnitBehaviour>());

            foreach (UnitBehaviour unitBehaviour in this.unitBehaviours)
            {
                unitBehaviour.Bake(this.unit);
            }
        }

        public void UnBake()
        {
            foreach (UnitBehaviour b in unitBehaviours)
                if(b != null)
                    b.UnBake();
            unitBehaviours.Clear();
        }
#endif
        #endregion

        public void CustomUpdate()
        {
            foreach (UnitBehaviour unitBehaviour in this.unitBehaviours)
                if (unitBehaviour.enabled)
                    unitBehaviour.CustomUpdate();
        }

        public void CustomFixedUpdate()
        {
            foreach (UnitBehaviour unitBehaviour in this.unitBehaviours)
                if (unitBehaviour.enabled)
                    unitBehaviour.CustomFixedUpdate();
        }

        public void CustomOnDestroy()
        {
            foreach (UnitBehaviour unitBehaviour in this.unitBehaviours)
                if (unitBehaviour.enabled)
                    unitBehaviour.CustomOnDestroy();
        }

        public void AddBehaviour(UnitBehaviour unitBehaviour)
        {
			unitBehaviour.Initialize(this.unit);
            unitBehaviours.Add(unitBehaviour);
        }

        public T AddNewBehaviour<T>()
            where T : UnitBehaviour
        {
            var newBehaviour = this.gameObject.AddComponent<T>();
            this.AddBehaviour(newBehaviour);
            return newBehaviour;
        }

        public void AddBehaviour(UnitBehaviour[] behaviours)
        {
            foreach (UnitBehaviour behaviour in behaviours)
                this.AddBehaviour(behaviour);
        }

        public UnitBehaviour[] AddNewBehaviours(UnitBehaviour[] behaviours)
        {
            List<UnitBehaviour> result = new List<UnitBehaviour>();
            foreach (UnitBehaviour behaviour in behaviours)
            {
                UnitBehaviour newBehaviour = (UnitBehaviour)this.gameObject.AddComponent(typeof(UnitBehaviour));
                this.AddBehaviour(newBehaviour);
                result.Add(newBehaviour);
            }
            return result.ToArray();
        }

        public T GetBehaviour<T>()
            where T : UnitBehaviour
        {
            foreach (UnitBehaviour behaviour in this.unitBehaviours)
                if (behaviour.GetType() == typeof(T))
                    return (T)behaviour;

            return null;
        }

        public T[] GetBehaviours<T>()
            where T : UnitBehaviour
        {
            List<T> result = new List<T>();
            foreach (UnitBehaviour behaviour in this.unitBehaviours)
                if (behaviour.GetType() == typeof(T))
                    result.Add((T)behaviour);

            return result.ToArray();
        }

        public List<UnitBehaviour> GetBehaviours(UnitBehaviour unitBehaviour)
        {
            List<UnitBehaviour> results = new List<UnitBehaviour>();

            foreach (UnitBehaviour iUnitBehaviour in this.unitBehaviours)
            {
                if (iUnitBehaviour.GetType() == unitBehaviour.GetType())
                {
                    results.Add(iUnitBehaviour);
                }
            }

            return results;
        }

        public bool RemoveBehaviour(UnitBehaviour unitBehaviour)
        {
            if (unitBehaviours.Contains(unitBehaviour))
            {
                unitBehaviours.Remove(unitBehaviour);
                Destroy(unitBehaviour);
                return true;
            }
            return false;
        }

        public bool RemoveBehaviour<T>()
            where T : UnitBehaviour
        {
            int length = unitBehaviours.Count;
            for (int i = 0; i < length; i++)
            {
                UnitBehaviour unitBehaviour = unitBehaviours[i];
                if (unitBehaviour.GetType() == typeof(T))
                {
                    unitBehaviours.Remove(unitBehaviour);
                    Destroy(unitBehaviour);
                    return true;
                }
            }
            return false;
        }
    }
}
