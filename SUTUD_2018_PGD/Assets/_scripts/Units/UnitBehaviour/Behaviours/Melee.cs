﻿using Sutud.Units.Properties;
using System.Collections.Generic;
using UnityEngine;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(AggroProperty))]
    public class Melee : UnitBehaviour
    {
        public float range;
        public float minDamage;
        public float maxDamage;
        public List<Sword> animationScripts;
        [HideInInspector][SerializeField]
        AggroProperty aggroProperty;

        private float AttackTime;
        public float RechargeTime = 1;

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            aggroProperty = unit.unitPropertiesManager.GetProperty<AggroProperty>();
            base.Bake(unit);
        }

        public override void UnBake()
        {
            aggroProperty = null;
            base.UnBake();
        }
#endif

        public override void CustomUpdate()
        {
            if (unit.target != null)
            {
                if (Vector3.Distance(unit.target.transform.position, this.transform.position) <= this.range)
                {
                    if (AttackTime <= Time.fixedTime)
                    {
                        AttackTime = Time.fixedTime + RechargeTime;
                        TakeDamageBehavior tdb = unit.target.unitBehaviourManager.GetBehaviour<TakeDamageBehavior>();
                        var damage = Random.Range(minDamage, maxDamage);
                        tdb.ReceiveDamage(damage, unit);
                        aggroProperty._value += damage;
                        foreach (Sword animationScript in animationScripts)
                            animationScript.Attack();//animate
                    }
                }
            }
        }

        public override void CustomOnDestroy()
        {

        }

        public override void CustomFixedUpdate()
        {
        }
    }
}