﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.Properties;


namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(AggroProperty))]
    [RequireComponent(typeof(ManaMaxProperty))]
    [RequireComponent(typeof(ManaProperty))]
    public class HealingBehaviour : UnitBehaviour
    {

        public float range;
        public float healAmount;
        public float manaCost;
        [SerializeField]
        private float attackTime;
        public float rechargeTime = .8f;

        [SerializeField]
        [HideInInspector]
        ManaProperty manaProperty;
        [SerializeField]
        [HideInInspector]
        OwnerIDProperty ownerIdProperty;
        [SerializeField]
        [HideInInspector]
        AggroProperty agroProperty;

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            base.Bake(unit);
            manaProperty = unit.unitPropertiesManager.GetProperty<ManaProperty>();
            ownerIdProperty = unit.unitPropertiesManager.GetProperty<OwnerIDProperty>();
            agroProperty = unit.unitPropertiesManager.GetProperty<AggroProperty>();
        }

        public override void UnBake()
        {
            base.UnBake();
            manaProperty = null;
            ownerIdProperty = null;
            agroProperty = null;
        }
#endif

        public override void CustomFixedUpdate()
        {
            if (unit.target != null)
            {
                if (unit.target.unitPropertiesManager.GetProperty<OwnerIDProperty>()._value == this.ownerIdProperty._value)
                {
                    if (unit.target.unitPropertiesManager.GetProperty<HealthProperty>()._value < unit.target.unitPropertiesManager.GetProperty<HealthMaxProperty>()._value)
                    {
                        if (Vector3.Distance(unit.target.transform.position, this.transform.position) <= this.range)
                        {
                            if (attackTime <= Time.fixedTime)
                            {
                                if (manaProperty._value >= manaCost)
                                {
                                    manaProperty._value -= manaCost;
                                    attackTime = Time.fixedTime + rechargeTime;
                                    unit.target.unitBehaviourManager.GetBehaviour<ReceiveHealingBehaviour>().ReceiveHealing(healAmount);
                                    agroProperty._value += this.healAmount;
                                }
                            }
                        }
                    }
                }
            }
        }


        public override void CustomOnDestroy() { }

        public override void CustomUpdate() { }
    }
}
