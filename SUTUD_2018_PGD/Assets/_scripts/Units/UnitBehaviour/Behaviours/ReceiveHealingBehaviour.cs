﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors
{
    [DisallowMultipleComponent]
    public class ReceiveHealingBehaviour : UnitBehaviour
    {
        // Use this for initialization
        //public float healAmount;
        [SerializeField]
        [HideInInspector]
        HealthProperty hpProp;
        [SerializeField]
        [HideInInspector]
        HealthMaxProperty hpMaxProp;
#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            base.Bake(unit);
            hpProp  = unit.unitPropertiesManager.GetProperty<HealthProperty>();
            hpMaxProp = unit.unitPropertiesManager.GetProperty<HealthMaxProperty>();
        }

        public override void UnBake()
        {
            base.UnBake();
            hpProp = null;
            hpMaxProp = null;
        }
#endif

        public override void CustomFixedUpdate()
        {
        }

        public override void CustomOnDestroy() { }

        public override void CustomUpdate() { }

        public virtual void ReceiveHealing(float healAmount)
        {
            this.hpProp._value += healAmount;
            if (hpProp._value >= hpMaxProp._value)
            {
                hpProp._value = hpMaxProp._value;
            }
        }
    }
}