﻿using Sutud.Units.Properties;
using UnityEngine;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(HealthProperty))]
    [RequireComponent(typeof(BountyProperty))]
    public class TakeDamageBehavior : UnitBehaviour
    {
        [SerializeField]
        [HideInInspector]
        BountyProperty bountyProperty;
        [SerializeField]
        [HideInInspector]
        HealthProperty hpProperty;


#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            bountyProperty = unit.unitPropertiesManager.GetProperty<BountyProperty>();
            hpProperty = unit.unitPropertiesManager.GetProperty<HealthProperty>();
            base.Bake(unit);
        }

        public override void UnBake()
        {
            bountyProperty = null;
            hpProperty = null;
            base.UnBake();
        }
#endif

        public override void CustomFixedUpdate()
        {
        }

        public override void CustomOnDestroy()
        {
        }

        public override void CustomUpdate()
        {
        }

        public virtual void ReceiveDamage(float damage, Unit attacker, AggroProperty attackersAgroProperty, bool isCrit = false)
        {
            attackersAgroProperty._value += damage;
            this.hpProperty._value -= damage;
			this.unit.UnitIsHit(attacker);
            if (this.hpProperty._value <= 0)
            {
                attacker.myPlayer.gold += bountyProperty._value;
                Destroy(this.gameObject);
            }

        }

        /// <summary>
        /// This version is a legacy version, 
        /// use ReceiveDamage(float damage, Unit attacker, AggroProperty attackersAgroProperty) instead
        /// </summary>
        /// <param name="damage">The damage.</param>
        /// <param name="attacker">The attacker.</param>
        public virtual void ReceiveDamage(float damage, Unit attacker)
        {
            Debug.LogError("Legacy version DO NOT USE");
            this.hpProperty._value -= damage;
            this.unit.UnitIsHit(attacker);
            if (this.hpProperty._value <= 0)
            {
                attacker.myPlayer.gold += bountyProperty._value;
                Destroy(this.gameObject);
            }

        }
    }
}
