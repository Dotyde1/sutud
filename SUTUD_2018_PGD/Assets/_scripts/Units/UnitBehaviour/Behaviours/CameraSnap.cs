﻿using Sutud.Units.Properties;
using UnityEngine;
using System.Collections.Generic;
using Sutud.Variables.RuntimeSets;
using System;

namespace Sutud.Cameras
{

	public class CameraSnap : MonoBehaviour
    {
        public CamController myController;
        [Range(0,1)]
        public float speed = .3f;
        public float snapRotationSpeed = 5f;

        public Dictionary<CameraSnapNames, CameraPoint> cameraPoints = new Dictionary<CameraSnapNames, CameraPoint>();
        Vector3 goalPosition;
        Quaternion GoalRotation;
        public bool moving = false;
        public CameraPointRunTimeSet snapPointsRunTimeSet;

        public void GetCameraPoint(CameraSnapNames snapTypeToGet, int playerIdToGet)
        {
            foreach(CameraPoint cp in snapPointsRunTimeSet.items)
            {
                if(cp.SnapName == snapTypeToGet && cp.PlayerID == playerIdToGet)
                {
                    cameraPoints.Add(snapTypeToGet, cp);
                    return;
                }
            }

            throw new NoCameraSnapPointFoundException(snapTypeToGet, playerIdToGet);
        }

        public void OnFightStart()
        {
            StartMoveToCameraSnapPoint(CameraSnapNames.Fight);
        }

        public void OnFightEnd()
        {
            StartMoveToCameraSnapPoint(CameraSnapNames.Market);
        }

		public void ShowUnits(){
			StartMoveToCameraSnapPoint (CameraSnapNames.ShowUnits);
		}

		public void ShowMelee(){
			StartMoveToCameraSnapPoint (CameraSnapNames.ShowMelee);
		}

		public void ShowBuilding(){
			StartMoveToCameraSnapPoint (CameraSnapNames.ShowBuilding);
		}
		public void PlaceUnit(){
			StartMoveToCameraSnapPoint (CameraSnapNames.PlaceUnit);
		}

        public void StartMoveToCameraSnapPoint(CameraSnapNames cameraPointEnum) 
        {
            if (!cameraPoints.ContainsKey(cameraPointEnum)) 
            {
                GetCameraPoint(cameraPointEnum, myController.playerID);
            }
            CameraPoint cameraPoint = cameraPoints[cameraPointEnum];
            goalPosition = cameraPoint.transform.position;
            GoalRotation = cameraPoint.transform.rotation;
            moving = true;
        }

        void FixedUpdate()
        {
            if (moving)
            {
                this.transform.position = Vector3.Lerp(this.transform.position, goalPosition, this.speed);
                transform.rotation = Quaternion.Lerp(transform.rotation, GoalRotation, this.speed);
                if ((this.transform.position - goalPosition).sqrMagnitude < 10 && transform.rotation == GoalRotation)
                {
                    moving = false;
                }
            }
        }

        public virtual void StopMoving() {
            moving = false;
        }

        //public override void StopMoving() {
            //do nothing, we want tutrual snap to always complete snaps
        //}
    }
}


