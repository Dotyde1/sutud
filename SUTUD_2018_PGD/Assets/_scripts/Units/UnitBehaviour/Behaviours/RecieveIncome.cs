﻿using Sutud.Units.Properties;
using UnityEngine;
namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(IncomeProperty))]
    public class RecieveIncome : UnitBehaviour
    {

        [HideInInspector][SerializeField]
        IncomeProperty incomeProperty;

        public override void Initialize()
        {
            base.Initialize(unit);
            unit.OnPurchase += OnPurchaseListener;
        }

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            incomeProperty = unit.unitPropertiesManager.GetProperty<IncomeProperty>();
            base.Bake(unit);
        }

        public override void UnBake()
        {
            incomeProperty = null;
            base.UnBake();
        }
#endif

        public override void CustomFixedUpdate()
        {
        }

        public override void CustomOnDestroy()
        {
        }

        public override void CustomUpdate()
        {
        }

        public void OnPurchaseListener(object sender, System.EventArgs args)
        {
            IncreaseIncome();
        }
        public virtual void IncreaseIncome()
        {
            unit.myPlayer.income += incomeProperty._value;
        }
    }
}
