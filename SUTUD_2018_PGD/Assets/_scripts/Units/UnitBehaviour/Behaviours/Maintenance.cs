﻿using Sutud.Units.Properties;
using UnityEngine;
namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(MaintenanceProperty))]
    public class Maintenance : UnitBehaviour {

        public MaintenanceProperty maintenanceProperty;

        public override void Initialize()
        {
            base.Initialize(unit);
            unit.OnPurchase += OnPurchaseListener;
        }

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            maintenanceProperty = unit.unitPropertiesManager.GetProperty<MaintenanceProperty>();
            base.Bake(unit);
        }

        public override void UnBake()
        {
            maintenanceProperty = null;
            base.UnBake();
        }
#endif

        public override void CustomFixedUpdate()
        {
        }

        public override void CustomOnDestroy()
        {
        }

        public override void CustomUpdate()
        {
        }

        public void OnPurchaseListener(object sender, System.EventArgs args)
        {
            IncreaseIncome();
        }
        public virtual void IncreaseIncome()
        {
            unit.myPlayer.maintenance += maintenanceProperty._value;
        }
    }
}

