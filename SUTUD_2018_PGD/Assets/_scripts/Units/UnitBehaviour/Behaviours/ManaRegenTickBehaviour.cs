﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(ManaProperty))]
    [RequireComponent(typeof(ManaRegenProperty))]
    [RequireComponent(typeof(ManaMaxProperty))]
    public class ManaRegenTickBehaviour : UnitBehaviour
    {

        private float regenTime;
        [Tooltip("Time between ticks in seconds")]
        public float tickRate = 1f;

        [SerializeField]
        [HideInInspector]
        ManaRegenProperty manaRegenProperty;
        [SerializeField]
        [HideInInspector]
        ManaProperty manaProperty;
        [SerializeField]
        [HideInInspector]
        ManaMaxProperty manaMaxProperty;

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            base.Bake(unit);
            manaRegenProperty = unit.unitPropertiesManager.GetProperty<ManaRegenProperty>();
            manaProperty = unit.unitPropertiesManager.GetProperty<ManaProperty>();
            manaMaxProperty = unit.unitPropertiesManager.GetProperty<ManaMaxProperty>();
        }

        public override void UnBake()
        {
            base.UnBake();
            manaRegenProperty = null;
            manaProperty = null;
            manaMaxProperty = null;
        }
#endif

        public override void CustomFixedUpdate()
        {
            regenTime -= Time.fixedDeltaTime;
            while (regenTime < 0)
            {
                if (manaProperty._value < manaMaxProperty._value)
                {
                    manaProperty._value += manaRegenProperty._value;
                    regenTime += tickRate;
                    if (manaProperty._value >= manaMaxProperty._value)
                    {
                        manaProperty._value = manaMaxProperty._value;
                    }
                }
                else
                {
                    regenTime = 0;
                }
            }
        }

        public override void CustomOnDestroy() { }

        public override void CustomUpdate() { }
    }
}
