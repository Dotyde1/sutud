﻿using Sutud.Units.Properties;
using UnityEngine;
namespace Sutud.Units.Behaviors
{
    public class DeathSoundSpawn : UnitBehaviour
    {

        public GameObject SoundObject;

        public override void Initialize()
        {
            base.Initialize(unit);

        }

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            base.Bake(unit);
        }

        public override void UnBake()
        {
            base.UnBake();
        }
#endif

        public override void CustomFixedUpdate() { }

        public override void CustomOnDestroy() 
        {
            SpawnSound();
        }

        public override void CustomUpdate() { }
        [ContextMenu("DebugCallSpawnSound")]
        public virtual void SpawnSound()
        {
            //if (SoundObject != null)
                //Instantiate(SoundObject, this.gameObject.transform.position, this.gameObject.transform.rotation);
        }

    }
}
