﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(ManaProperty))]
    [RequireComponent(typeof(ManaRegenProperty))]
    [RequireComponent(typeof(ManaMaxProperty))]
    public class ManaRegenOverTimeBehaviour : UnitBehaviour
    {
        [SerializeField]
        [HideInInspector]
        ManaRegenProperty manaRegenProperty;
        [SerializeField]
        [HideInInspector]
        ManaProperty manaProperty;
        [SerializeField]
        [HideInInspector]
        ManaMaxProperty manaMaxProperty;

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            base.Bake(unit);
            manaRegenProperty = unit.unitPropertiesManager.GetProperty<ManaRegenProperty>();
            manaProperty = unit.unitPropertiesManager.GetProperty<ManaProperty>();
            manaMaxProperty = unit.unitPropertiesManager.GetProperty<ManaMaxProperty>();
        }

        public override void UnBake()
        {
            base.UnBake();
            manaRegenProperty = null;
            manaProperty = null;
            manaMaxProperty = null;
        }
#endif

        public override void CustomFixedUpdate()
        {
            manaProperty._value += manaRegenProperty._value * Time.fixedDeltaTime;
            if (manaProperty._value > manaMaxProperty._value)
            {
                manaProperty._value = manaMaxProperty._value;
            }
        }

        public override void CustomOnDestroy() { }

        public override void CustomUpdate() { }
    }
}
