﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors
{
    public class AggroPropertyBehavior : UnitBehaviour
    {
        [SerializeField]
        [HideInInspector]
        private AggroProperty aggroProperty;

        public float defaultDecreaseRatePerSecond = 1;
        public float decreaseRatePerSecond = 1;
        [Range(0, 1)]
        public float exponent = .1f;

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            aggroProperty = unit.unitPropertiesManager.GetProperty<AggroProperty>();
            base.Bake(unit);
        }

        public override void UnBake()
        {
            aggroProperty = null;
            base.UnBake();
        }
#endif
        public override void CustomFixedUpdate()
        {
            if(aggroProperty._value > 0)
                decreaseRatePerSecond = Mathf.Pow(aggroProperty._value, exponent) * defaultDecreaseRatePerSecond;
            aggroProperty._value -= decreaseRatePerSecond * Time.deltaTime;
            if (aggroProperty._value < 0 || aggroProperty._value == float.NaN)
            {
                aggroProperty._value = 0;
            }
        }

        public override void CustomOnDestroy()
        {
        }

        public override void CustomUpdate()
        {
        }
    }
}
