﻿using Sutud.Units.Properties;
using UnityEngine;
using System.Collections;
namespace Sutud.Units.Behaviors
{
    public class DyingSoundBehavior : MonoBehaviour
    {


        public AudioClip[] deathSoundlist;
        public AudioSource deathSoundSource;

         void Start()
        {
            deathSoundSource.clip = deathSoundlist[Random.Range(0, deathSoundlist.Length)];
            deathSoundSource.pitch = (Random.Range(0.75f, 2f));
            PlaySound();
            StartCoroutine(PlaySound());
        }

        public IEnumerator PlaySound()
        {
            deathSoundSource.Play();
            yield return new WaitForSeconds(deathSoundSource.clip.length);
            Destroy(this.gameObject);
        }
    }
}