﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors
{
	public class FleeingBehaviour : UnitBehaviour
	{
		[SerializeField]
        [HideInInspector]
        private Melee meleeBehaviour;
		[SerializeField]
        [HideInInspector]
        private RangedAttackBehavior rangedBehaviour;
		[SerializeField]
        [HideInInspector]
        private WalkingBehavior walkBehaviour;
		[SerializeField]
        [HideInInspector]
        private MovementSpeedProperty moveProp;

		private Transform targetTransform;
		private Transform startTransform;
		[SerializeField]
		private NavMeshAgent navAgent;

		public float timeToFlee;
		public float timeRemainingToFlee;

		public static bool isFleeing;
		public bool shouldFleeOnHit = false;

		public Vector3 runTo;

		public override void Initialize ()
		{
			base.Initialize ();
			if (shouldFleeOnHit) {
				unit.OnHit += OnHitListener;
			}
        
		}

		#if UNITY_EDITOR
		public override void Bake (Unit unit)
		{
			base.Bake (unit);
			meleeBehaviour = unit.unitBehaviourManager.GetBehaviour<Melee> ();
			rangedBehaviour = unit.unitBehaviourManager.GetBehaviour<RangedAttackBehavior> ();
			walkBehaviour = unit.unitBehaviourManager.GetBehaviour<WalkingBehavior> ();
			moveProp = unit.unitPropertiesManager.GetProperty<MovementSpeedProperty> ();
			navAgent = this.GetComponent<NavMeshAgent> ();
		}

		public override void UnBake ()
		{
			base.UnBake ();
			meleeBehaviour = null;
			rangedBehaviour = null;
			walkBehaviour = null;
			moveProp = null;
			navAgent = null;
		}
		#endif

		public override void CustomFixedUpdate ()
		{ 
			if(isFleeing)
			{
				timeRemainingToFlee -= Time.fixedDeltaTime;
				if(timeRemainingToFlee <= 0)
				{
					StopFleeing();
				}
			}
		}

		public override void CustomOnDestroy ()
		{
		}

		public override void CustomUpdate ()
		{
		}

		public void OnHitListener (object sender, Unit.UnitEventArg arg)
		{
			StartFleeing(arg.unit);
		}

		public void StartFleeing (float timeToFlee)
		{
			if(timeToFlee > timeRemainingToFlee)
				timeRemainingToFlee = timeToFlee;
			isFleeing = true;
			if (walkBehaviour != null) {
				walkBehaviour.enabled = false;
			}
			if (meleeBehaviour != null) {
				meleeBehaviour.enabled = false;
			}
			if (rangedBehaviour != null) {
				rangedBehaviour.enabled = false;
			}
	        	
			startTransform = unit.transform;

			Vector3 runTo = transform.position + transform.forward * moveProp._value * timeRemainingToFlee;

			NavMeshHit hit;

			NavMesh.SamplePosition (runTo, out hit, 5, 1 << NavMesh.GetNavMeshLayerFromName ("Default"));

			unit.transform.position = startTransform.position;

			navAgent.SetDestination (hit.position);
		}

		public void StartFleeing (float timeToFlee, Unit unitToFleeFrom)
		{
			isFleeing = true;
			if(timeToFlee > timeRemainingToFlee)
				timeRemainingToFlee = timeToFlee;
			if (walkBehaviour != null) {
				walkBehaviour.enabled = false;
			}
			if (meleeBehaviour != null) {
				meleeBehaviour.enabled = false;
			}
			if (rangedBehaviour != null) {
				rangedBehaviour.enabled = false;
			}
	        	
			startTransform = unit.transform;
			targetTransform = unitToFleeFrom.transform;

			runTo = transform.position + (transform.position-targetTransform.position).normalized * moveProp._value * timeRemainingToFlee;

			NavMeshHit hit;

			NavMesh.SamplePosition (runTo, out hit, 5, 1 << NavMesh.GetNavMeshLayerFromName("Default"));

			unit.transform.position = startTransform.position;

			navAgent.SetDestination (hit.position);
		}

		public void StartFleeing (Unit unitToFleeFrom)
		{
	        StartFleeing(timeToFlee, unitToFleeFrom);
		}

		public void StopFleeing(){
			isFleeing = false;
			if (walkBehaviour != null) {
				walkBehaviour.enabled = true;
			}
			if (meleeBehaviour != null) {
				meleeBehaviour.enabled = true;
			}
			if (rangedBehaviour != null) {
				rangedBehaviour.enabled = true;
			}
		}
	}
}