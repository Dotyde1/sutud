﻿using UnityEngine;
using UnityEngine.AI;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(MovementSpeedProperty))]
    public class WalkingBehavior : UnitBehaviour
    {
        [SerializeField]
        [HideInInspector]
        NavMeshAgent agent;
        /*[SerializeField]
        Vector3 goalPossition = Vector3.zero;*/

        [SerializeField]
        [HideInInspector]
        Unit trackingUnit = null;

        //Property references
        [SerializeField]
        [HideInInspector]
        MovementSpeedProperty movementSpeedProperty;

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            agent = GetComponent<NavMeshAgent>();
            movementSpeedProperty = unit.unitPropertiesManager.GetProperty<MovementSpeedProperty>();
            base.Bake(unit);
        }

        public override void UnBake()
        {
            agent = null;
            movementSpeedProperty = null;
            base.UnBake();
        }
#endif

		public override void Initialize()
        {

            unit.OnChangedTarget += OnTargetChangeListener;

			base.Initialize();
        }

        public override void CustomFixedUpdate()
        {
            this.agent.speed = movementSpeedProperty._value;
            if (this.trackingUnit != null)
            {
                agent.SetDestination(trackingUnit.transform.position);
            }
            else
            {
                if (agent.isOnNavMesh)
                    agent.destination = new Vector3(0, 0, 0);
                else
                {
                    this.agent.enabled = false;
                    this.agent.enabled = true;
                }
            }
        }

        public override void CustomOnDestroy()
        {
        }

        public override void CustomUpdate()
        {
        }

        public override void OnStateChangeListener(object sender, Unit.UnitStateEventArg eventArg)
        {
            base.OnStateChangeListener(sender, eventArg);
            agent.enabled = this.enabled;
        }

        public void OnTargetChangeListener(object sender, Unit.UnitEventArg args)
        {
            this.trackingUnit = args.unit;
        }
    }
}
