﻿using Sutud.Units.Projectiles;
using Sutud.Units.Properties;
using UnityEngine;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(AggroProperty))]
    public class RangedAttackBehavior : UnitBehaviour
    {

        public GameObject projectilePrefab;

        public float projectileSpeed = 1;
        public float range;
        public float minDamage;
        public float maxDamage;

        private float AttackTime;
        public float RechargeTime = 1;

        [HideInInspector][SerializeField]
        AggroProperty aggroProperty;


#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            aggroProperty = unit.unitPropertiesManager.GetProperty<AggroProperty>();
            base.Bake(unit);
        }

        public override void UnBake()
        {
            aggroProperty = null;
            base.UnBake();
        }
#endif

        public override void CustomFixedUpdate()
        {
            if(aggroProperty == null)
                aggroProperty = unit.unitPropertiesManager.GetProperty<AggroProperty>();

            if (unit.target != null)
            {
                if (Vector3.Distance(unit.target.transform.position, this.transform.position) <= this.range)
                {
                    if (AttackTime <= Time.fixedTime)
                    {
                        AttackTime = Time.fixedTime + RechargeTime;
                        var damage = UnityEngine.Random.Range(minDamage, maxDamage);
                        aggroProperty._value += damage;
                        Instantiate(projectilePrefab, transform.position, Quaternion.identity).GetComponent<Projectile>().Initialize(damage, projectileSpeed, unit.target, unit);
                    }
                }
            }
        }

        public override void CustomOnDestroy()
        {
        }

        public override void CustomUpdate()
        {
        }
    }
}
