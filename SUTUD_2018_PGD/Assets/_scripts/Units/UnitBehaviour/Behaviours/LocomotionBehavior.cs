﻿using UnityEngine;
using Sutud.Units.Properties;
using System;
using UnityEngine.AI;

namespace Sutud.Units.Behaviors
{ 
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(MovementSpeedProperty))]
    [RequireComponent(typeof(RotationSpeedProperty))]
    public class LocomotionBehavior : UnitBehaviour
    {
        [HideInInspector]
        [SerializeField] Rigidbody unitRigidBody;
        [HideInInspector]
        [SerializeField] Animator animator;
        [HideInInspector]
        [SerializeField] MovementSpeedProperty movementSpeedProperty;
        [HideInInspector]
        [SerializeField] RotationSpeedProperty rotationSpeedProperty;
        [HideInInspector]
        [SerializeField] NavMeshAgent agent;
        [HideInInspector]
        [SerializeField] CapsuleCollider capsule;
        [HideInInspector]
        [SerializeField] float capsuleHeight;
        [HideInInspector]
        [SerializeField] Vector3 capsuleCenter;

        [SerializeField] float m_JumpPower = 6f;
        [Range(1f, 4f)] [SerializeField] float m_GravityMultiplier = 2f;
        [SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
        [SerializeField] float m_MoveSpeedMultiplier = 1f;
        [SerializeField] float m_AnimSpeedMultiplier = 1f;
        [SerializeField] float m_GroundCheckDistance = 0.2f;



        bool isGrounded;
        float origGroundCheckDistance;
        const float half = 0.5f;
        float turnAmount;
        float forwardAmount;
        Vector3 groundNormal;

        bool crouching;

#if UNITY_EDITOR
        public override void Bake(Unit unit)
        {
            base.Bake(unit);
            unitRigidBody = GetComponent<Rigidbody>();
            this.animator = GetComponent<Animator>();
            capsule = GetComponent<CapsuleCollider>();
            capsuleHeight = capsule.height;
            capsuleCenter = capsule.center;
            movementSpeedProperty = this.unit.unitPropertiesManager.GetProperty<MovementSpeedProperty>();
            rotationSpeedProperty = this.unit.unitPropertiesManager.GetProperty<RotationSpeedProperty>();
            agent = GetComponent<NavMeshAgent>();
            unitRigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }

        public override void UnBake()
        {
            base.UnBake();
            if (unitRigidBody != null)
                unitRigidBody.constraints = RigidbodyConstraints.None;
            unitRigidBody = null;
            this.animator = null;
            capsule = null;
            capsuleHeight = 0;
            capsuleCenter = Vector3.zero;
            movementSpeedProperty = null;
            rotationSpeedProperty = null;
            agent = null;

        }
#endif

        void Start()
        {

            origGroundCheckDistance = m_GroundCheckDistance;
        }

        public override void Initialize()
        {
            base.Initialize();
        }


        public void Move(Vector3 move, bool crouch, bool jump)
        {

            // convert the world relative moveInput vector into a local-relative
            // turn amount and forward amount required to head in the desired
            // direction.
            if (move.magnitude > 1f) move.Normalize();
            move = transform.InverseTransformDirection(move);
            CheckGroundStatus();
            move = Vector3.ProjectOnPlane(move, groundNormal);
            turnAmount = Mathf.Atan2(move.x, move.z);
            forwardAmount = move.z;

            ApplyExtraTurnRotation();

            // control and velocity handling is different when grounded and airborne:
            if (isGrounded)
            {
                HandleGroundedMovement(crouch, jump);
            }
            else
            {
                HandleAirborneMovement();
            }

            ScaleCapsuleForCrouching(crouch);
            PreventStandingInLowHeadroom();

            // send input and other state parameters to the animator
            UpdateAnimator(move);
        }


        void ScaleCapsuleForCrouching(bool crouch)
        {
            if (isGrounded && crouch)
            {
                if (crouching) return;
                capsule.height = capsule.height / 2f;
                capsule.center = capsule.center / 2f;
                crouching = true;
            }
            else
            {
                Ray crouchRay = new Ray(unitRigidBody.position + Vector3.up * capsule.radius * half, Vector3.up);
                float crouchRayLength = capsuleHeight - capsule.radius * half;
                if (Physics.SphereCast(crouchRay, capsule.radius * half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
                {
                    crouching = true;
                    return;
                }
                capsule.height = capsuleHeight;
                capsule.center = capsuleCenter;
                crouching = false;
            }
        }

        void PreventStandingInLowHeadroom()
        {
            // prevent standing up in crouch-only zones
            if (!crouching)
            {
                Ray crouchRay = new Ray(unitRigidBody.position + Vector3.up * capsule.radius * half, Vector3.up);
                float crouchRayLength = capsuleHeight - capsule.radius * half;
                if (Physics.SphereCast(crouchRay, capsule.radius * half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
                {
                    crouching = true;
                }
            }
        }


        void UpdateAnimator(Vector3 move)
        {
            // update the animator parameters
            animator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
            animator.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);
            animator.SetBool("Crouch", crouching);
            animator.SetBool("OnGround", isGrounded);
            if (!isGrounded)
            {
                animator.SetFloat("Jump", unitRigidBody.velocity.y);
            }

            // calculate which leg is behind, so as to leave that leg trailing in the jump animation
            // (This code is reliant on the specific run cycle offset in our animations,
            // and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
            float runCycle =
                Mathf.Repeat(
                    animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);
            float jumpLeg = (runCycle < half ? 1 : -1) * forwardAmount;
            if (isGrounded)
            {
                animator.SetFloat("JumpLeg", jumpLeg);
            }

            // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
            // which affects the movement speed because of the root motion.
            if (isGrounded && move.magnitude > 0)
            {
                animator.speed = m_AnimSpeedMultiplier;
            }
            else
            {
                // don't use that while airborne
                animator.speed = 1;
            }
        }


        void HandleAirborneMovement()
        {
            // apply extra gravity from multiplier:
            Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
            unitRigidBody.AddForce(extraGravityForce);

            m_GroundCheckDistance = unitRigidBody.velocity.y < 0 ? origGroundCheckDistance : 0.01f;
        }


        void HandleGroundedMovement(bool crouch, bool jump)
        {
            // check whether conditions are right to allow a jump:
            if (jump && !crouch && animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
            {
                // jump!
                unitRigidBody.velocity = new Vector3(unitRigidBody.velocity.x, m_JumpPower, unitRigidBody.velocity.z);
                isGrounded = false;
                animator.applyRootMotion = false;
                m_GroundCheckDistance = 0.1f;
            }
        }

        void ApplyExtraTurnRotation()
        {
            // help the character turn faster (this is in addition to root rotation in the animation)
            float turnSpeed = Mathf.Lerp(rotationSpeedProperty._value, movementSpeedProperty._value, forwardAmount);
            transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
        }


        public void OnAnimatorMove()
        {
            // we implement this function to override the default root motion.
            // this allows us to modify the positional speed before it's applied.
            if (isGrounded && Time.deltaTime > 0)
            {
                Vector3 v = (animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

                // we preserve the existing y part of the current velocity.
                agent.velocity = Vector3.zero;
                v.y = unitRigidBody.velocity.y;
                unitRigidBody.velocity = v;
            }
        }


        void CheckGroundStatus()
        {
            RaycastHit hitInfo;
#if UNITY_EDITOR
            // helper to visualise the ground check ray in the scene view
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
            // 0.1f is a small offset to start the ray from inside the character
            // it is also good to note that the transform position in the sample assets is at the base of the character
            if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
            {
                groundNormal = hitInfo.normal;
                isGrounded = true;
                animator.applyRootMotion = true;
            }
            else
            {
                isGrounded = false;
                groundNormal = Vector3.up;
                animator.applyRootMotion = false;
            }
        }

        public override void CustomUpdate()
        {
            //update agent variables with units current runtime variables
            agent.speed = this.movementSpeedProperty._value;

            //animate and move the unit
            if (agent.remainingDistance > agent.stoppingDistance)
                Move(agent.desiredVelocity, false, false);
            else
                Move(Vector3.zero, false, false);
        }

        public override void CustomFixedUpdate()
        {
        }

        public override void CustomOnDestroy()
        {
        }
    }
}
