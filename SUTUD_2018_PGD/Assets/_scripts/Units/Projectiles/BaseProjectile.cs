﻿using System.Collections;
using System.Collections.Generic;
using Sutud.Units;
using Sutud.Units.Behaviors;
using Sutud.Units.Properties;
using UnityEngine;
using Sutud.Units.Weapons;

namespace Sutud.Units.Projectiles
{
    public class BaseProjectile : MonoBehaviour
    {
        public AnimationCurve heightCurve;
        public float projectileHeight;
        public float projectileSpeed;
        public float projectileAcceleration;
        [SerializeField]
        protected Vector3 startPosition;
        [SerializeField]
        protected Vector3 goalPosition;
        [SerializeField]
        protected Vector2 startPosition2D;
        [SerializeField]
        protected Vector2 goalPosition2D;
        [SerializeField]
        protected Vector2 currentPosition2D;
        [SerializeField]
        protected TakeDamageBehavior targetDamageBehavior;
        [SerializeField]
        protected float progress = 0, maxRangePercent;
        protected float damage;
        protected bool isCrit;
        protected Weapon usedWeapon;
        [SerializeField]
        protected Unit caster;
        protected AggroProperty castersAgroProperty;

        public float totalDistance;
        public float distanceTravled;

        protected void Start()
        {
            projectileSpeed = Mathf.Abs(projectileSpeed);
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            projectileSpeed += projectileAcceleration * Time.fixedDeltaTime;
            UpdateGoalPosition();
            MoveToGoal();
            currentPosition2D = new Vector2(this.transform.position.x, this.transform.position.z);
            //TODO Improve logic to still be acurate but les costly (sqrMagnitude DOES NOT WORK!)
            distanceTravled = (startPosition2D - currentPosition2D).magnitude;
            //distanceRemaining = (currentPosition2D - goalPosition2D);
            progress = distanceTravled / totalDistance;
            UpdateProjectileHeight();
            if (CheckIfHit())
            {
                this.HitTarget();
            }
        }



        public virtual void Initialize(Unit caster, AggroProperty castersAgroProperty, Vector3 startPosition, Vector3 goalPosition, TakeDamageBehavior targetDamageBehavior, float damage, bool isCrit, Weapon usedWeapon, float maxRangePercent)
        {
            this.transform.position = startPosition;
            this.startPosition = startPosition;
            this.startPosition2D = new Vector2(startPosition.x, startPosition.z);
            currentPosition2D = startPosition2D;
            this.goalPosition = goalPosition;
            this.goalPosition2D = new Vector2(goalPosition.x, goalPosition.z);
            this.targetDamageBehavior = targetDamageBehavior;
            this.damage = damage;
            this.isCrit = isCrit;
            this.caster = caster;
            this.castersAgroProperty = castersAgroProperty;
            this.usedWeapon = usedWeapon;
            this.maxRangePercent = maxRangePercent;
            totalDistance = (startPosition2D - goalPosition2D).magnitude;
        }

        public virtual void MoveToGoal()
        {
            Vector2 direction = (goalPosition2D - currentPosition2D).normalized;
			Vector2 velocity = direction * projectileSpeed * Time.fixedDeltaTime;
            this.transform.position += new Vector3(velocity.x, 0, velocity.y);
        }

        /// <summary>
        /// Updates the goal position. Note that if you implement this method you must update the totalDistance value through:
        /// totalDistance = (startPosition2D - goalPosition2D).magnitude;
        /// </summary>
        public virtual void UpdateGoalPosition()
        {
        }

        public virtual void HitTarget()
        {
            if (usedWeapon.AoeRange == 0)
            {
                if (targetDamageBehavior != null)
                    if ((this.transform.position - this.targetDamageBehavior.transform.position).sqrMagnitude < projectileSpeed * projectileSpeed * Time.fixedDeltaTime)
                        targetDamageBehavior.ReceiveDamage(this.damage, this.caster, this.castersAgroProperty, this.isCrit);
            }
            else
            {

                foreach (PlayerManager enemiePlayer in caster.myPlayer.opponents)
                {
                    foreach (Unit enemieUnit in enemiePlayer.aliveUnits)
                    {
                        float distanceToAoeTarget = (enemieUnit.transform.position - this.transform.position).sqrMagnitude;
                        if (distanceToAoeTarget < usedWeapon.AoeRange)
                        {
                            var takeDamageBehavior = enemieUnit.unitBehaviourManager.GetBehaviour<TakeDamageBehavior>();
                            if (takeDamageBehavior != null)
                            {
                                takeDamageBehavior.ReceiveDamage(usedWeapon.CalculateSplashDamage(damage, distanceToAoeTarget), this.caster, castersAgroProperty, isCrit);
                            }
                        }
                    }
                }
            }

            DestroyProjectile();
        }

        public virtual bool CheckIfHit()
        {
			return (currentPosition2D - this.goalPosition2D).sqrMagnitude < projectileSpeed * projectileSpeed * Time.fixedDeltaTime;
        }

        public virtual void DestroyProjectile()
        {
            Destroy(this.gameObject);
        }

        private void UpdateProjectileHeight()
        {
            float height = heightCurve.Evaluate(progress) * maxRangePercent * projectileHeight;
            float startHeight = startPosition.y;
            float goalHeight = goalPosition.y;
            float curHeight = startHeight * (1 - progress) + goalHeight * progress + height;
            Vector3 heightUpdate = new Vector3(this.transform.position.x, curHeight, this.transform.position.z);
            this.transform.position = heightUpdate;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            if (caster != null)
                Gizmos.DrawLine(this.transform.position, caster.transform.position);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(this.transform.position, goalPosition);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(this.transform.position, startPosition);
        }
    }
}
