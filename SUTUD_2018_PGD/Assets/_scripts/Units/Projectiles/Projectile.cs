﻿using Sutud.Units.Behaviors;
using System;
using UnityEngine;

namespace Sutud.Units.Projectiles
{
    public class Projectile : MonoBehaviour
    {

        Unit target, attacker;
        float damage, speed;
        Vector3 goalPossition;
        
        public void Initialize(float damage, float speed, Unit target, Unit attacker)
        {
            this.target = target;
            this.damage = damage;
            this.speed = speed;
            this.attacker = attacker;
        }

        private void FixedUpdate()
        {
            if(target == null)
            {
                DestroySelf();
            }
            try
            {
                goalPossition = target.transform.position;
                this.transform.LookAt(goalPossition);

                var currentPosstion = transform.position;
                var frameMovementSpeed = speed * Time.fixedDeltaTime;

                if (Vector3.Distance(goalPossition, currentPosstion) > frameMovementSpeed)
                {
                    var relativeGoalDirection = (goalPossition - currentPosstion).normalized;
                    var newpos = (currentPosstion + relativeGoalDirection * frameMovementSpeed);
                    this.transform.position = newpos;
                }
                else
                {
                    target.unitBehaviourManager.GetBehaviour<TakeDamageBehavior>().ReceiveDamage(damage, attacker);
                    DestroySelf();
                }
            } catch (Exception e)
            {
                Debug.LogWarning(e.StackTrace);
                DestroySelf();
            }
        }

        void DestroySelf()
        {
            Destroy(this.gameObject);
        }

    }
}