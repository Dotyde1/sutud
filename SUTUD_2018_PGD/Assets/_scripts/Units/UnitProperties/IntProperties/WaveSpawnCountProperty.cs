﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [DisallowMultipleComponent]
    public class WaveSpawnCountProperty : IntPropertySuperClass
    {
#if UNITY_EDITOR
        [Multiline]
        public string developerTooltip = "This value defines the amount of units of this type should spawn per unit purchased";
#endif
    }
}
