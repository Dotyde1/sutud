﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [RequireComponent(typeof(HealthMaxProperty))][RequireComponent(typeof(HealthRegenProperty))]
    [AddComponentMenu("Properties/Float Properties/HealthProperty")]
    public class HealthProperty : FloatPropertySuperClass { }
}