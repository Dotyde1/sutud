﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sutud.Units.Properties
{
    public class SightDistanceProperty : FloatPropertySuperClass
    {
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(this.transform.position, Mathf.Sqrt(this._value));
        }
    }
}
