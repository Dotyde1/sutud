﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [RequireComponent(typeof(ShieldMaxProperty))]
    [RequireComponent(typeof(ShieldRegenProperty))]
    [AddComponentMenu("Properties/Float Properties/ShieldProperty")]
    public class ShieldProperty : FloatPropertySuperClass { }
}