﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [AddComponentMenu("Properties/Float Properties/AggroProperty")]
    public class AggroProperty : FloatPropertySuperClass
    {
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.up * this._value / 20);
        }
    }
}