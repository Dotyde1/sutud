﻿using UnityEngine;
namespace Sutud.Units.Properties
{
	[RequireComponent(typeof(ManaMaxProperty))][RequireComponent(typeof(ManaRegenProperty))]
    [AddComponentMenu("Properties/Float Properties/ManaProperty")]
    public class ManaProperty : FloatPropertySuperClass { }
}