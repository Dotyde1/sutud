﻿namespace Sutud.Units.Properties
{
    /// <summary>
    /// If possible, use the already existing possition vector in the GameObject!
    /// </summary>
    public class PossitionProperty : Vector3PropertySuperclass
    {
    }
}
