﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [AddComponentMenu("Properties/RaceProperty")]
    [DisallowMultipleComponent]
    public class RaceProperty : MonoBehaviour, IUnitProperty<Race> {
        [SerializeField]
        Race value;

        public Race GetComponentValue()
        {
            return value;
        }

        public void SetComponentValue(Race value)
        {
            this.value = value;
        }
    }
}
