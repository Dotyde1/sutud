﻿namespace Sutud.Units.Properties
{
    /// <summary>
    /// If possible, use the value already stored in the GameObject instead of this rotationQuaterion!
    /// </summary>
    public class RotationProperty : QuaternionPropertySuperClass
    {
    }
}