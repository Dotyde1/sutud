﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Sutud.Units.Properties
{
    public interface IUnitProperty<T> : IProperty
    {
        T GetComponentValue();

        void SetComponentValue(T value);
    }
}
