﻿namespace Sutud.Units.Properties
{
    public interface IIntUnitProperty : IUnitProperty<int> { }
}