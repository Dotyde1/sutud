﻿namespace Sutud.Units.Properties
{
    public interface IFloatUnitProperty : IUnitProperty<float> { }
}