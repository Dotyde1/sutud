﻿namespace Sutud.Units.Properties
{
    public interface IBoolUnitProperty : IUnitProperty<bool> { }
}