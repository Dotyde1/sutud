﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    public interface IQuaternionProperty : IUnitProperty<Quaternion>
    {
    }
}
