﻿namespace Sutud.Units.Properties
{
    public interface IStringUnitProperty : IUnitProperty<string> { }
}