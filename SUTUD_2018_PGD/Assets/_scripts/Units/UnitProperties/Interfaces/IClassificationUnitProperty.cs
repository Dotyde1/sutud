﻿namespace Sutud.Units.Properties
{
    public interface IClassificationUnitProperty : IUnitProperty<ClassificationEntry> { }
}