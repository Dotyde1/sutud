﻿using System.Collections.Generic;
using UnityEngine;
namespace Sutud.Units.Properties
{
    [AddComponentMenu("Properties/UnitPropertiesManager")]
    [RequireComponent(typeof(Unit))]
    public class UnitPropertiesManager : MonoBehaviour
    {

        /// <summary>
        /// List of all properties this manager is maintaining
        /// </summary>
        [SerializeField]
        public List<UnitPropertySuper> unitProperties = new List<UnitPropertySuper>();

#if UNITY_EDITOR
        [SerializeField]
        public int propertyCount = 0;

        public void Bake()
        {
            enabled = true;
            unitProperties = new List<UnitPropertySuper>(GetComponents<UnitPropertySuper>());
            propertyCount = unitProperties.Count;
        }

        public void UnBake()
        {
            unitProperties.Clear();
            propertyCount = unitProperties.Count;
        }

        void FixedUpdate()
        {
            propertyCount = unitProperties.Count;
        }

        //Set of debugging and testing methods
        [ContextMenu("Log all components and their values")]
        void LogAllComponents()
        {
            string log = "We have a total of " + unitProperties.Count + " Properties";
            int propCount = 1;
            foreach(IProperty iprop in unitProperties)
            {
                try
                {
                    var prop = (IFloatUnitProperty)iprop;
                    log += string.Format("\n{0,5} {1,-60}: {2}", propCount, prop.GetType().ToString(), prop.GetComponentValue());
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IBoolUnitProperty)iprop;
                    log += string.Format("\n{0,5} {1,-60}: {2}", propCount, prop.GetType().ToString(), prop.GetComponentValue());
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IStringUnitProperty)iprop;
                    log += string.Format("\n{0,5} {1,-60}: {2}", propCount, prop.GetType().ToString(), prop.GetComponentValue());
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (RaceProperty)iprop;
                    log += string.Format("\n{0,5} {1,-60}: {2}", propCount, prop.GetType().ToString(), prop.GetComponentValue());
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IClassificationUnitProperty)iprop;
                    log += string.Format("\n{0,5} {1,-60}: {2}, {3}", propCount, prop.GetType().ToString(), 
                        prop.GetComponentValue().classification, prop.GetComponentValue().signifiganceOfClassifiation);
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IQuaternionProperty)iprop;
                    log += string.Format("\n{0,5} {1,-60}: x:{2}, y:{3}, z:{4}, w:{5}", propCount, prop.GetType().ToString(),
                        prop.GetComponentValue().x, prop.GetComponentValue().y, prop.GetComponentValue().z, prop.GetComponentValue().w);
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IVector3Property)iprop;
                    log += string.Format("\n{0,5} {1,-60}: x:{2}, y:{3}, z:{4}", propCount, prop.GetType().ToString(),
                        prop.GetComponentValue().x, prop.GetComponentValue().y, prop.GetComponentValue().z);
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }

                propCount++;
            }
            Debug.Log(log);
        }

        [ContextMenu("Randomize component values")]
        void SetAllCompomentsValuesToRandomValue()
        {
            foreach (IProperty iprop in unitProperties)
            {
                try
                {
                    var prop = (IFloatUnitProperty)iprop;
                    prop.SetComponentValue(Random.Range(float.MinValue, float.MaxValue));
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IBoolUnitProperty)iprop;
                    prop.SetComponentValue(Random.Range(0, 2)==0);
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IStringUnitProperty)iprop;
                    char[] randomChars = new char[Random.Range(2, 200)];
                    for (int i = 0; i < randomChars.Length; i++)
                    {
                        randomChars[i] = (char)Random.Range(' ', 'z');
                    }
                    prop.SetComponentValue(new string(randomChars));
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (RaceProperty)iprop;
                    //Change last value here too the last Race value for better testing :D
                    prop.SetComponentValue((Race)Random.Range((int)Race.None, (int)Race.None));
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IClassificationUnitProperty)iprop;
                    prop.SetComponentValue(new ClassificationEntry((UnitClassifications)Random.Range(0,(int)UnitClassifications.last), Random.Range(float.MinValue, float.MaxValue)));
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IQuaternionProperty)iprop;
                    prop.SetComponentValue(new Quaternion(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)));
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
                try
                {
                    var prop = (IVector3Property)iprop;
                    prop.SetComponentValue(new Vector3(Random.Range(-200f, 200f), Random.Range(-200f, 200f), Random.Range(-200f, 200f)));
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (System.Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                }
            }
        }
#endif

        /// <summary>
        /// Intializes the unitProperties list. Based purely on instances found on this gameobject!
        /// </summary>
        [ContextMenu("(re)Intialize")]
        public void Initialize()
        {

        }

        /// <summary>
        /// Add a property too this PropertiesManager
        /// </summary>
        /// <param name="property">Property being added</param>
        public void AddProperty(UnitPropertySuper property)
        {
            unitProperties.Add(property);
        }

        /// <summary>
        /// Adds a new instance of Property T too the gameobject, then adds the new property too this PropertiesManager
        /// </summary>
        /// <typeparam name="T">Type of the property you whish to add</typeparam>
        /// <returns>The new property instance for further tweaking of that instance</returns>
        public T AddNewProperty<T>()
            where T : UnitPropertySuper
        {
            var newProp = this.gameObject.AddComponent<T>();
            this.AddProperty(newProp);
            return newProp;
        }

        /// <summary>
        /// Adds all properties from the collection to this PropertiesManager
        /// </summary>
        /// <param name="properties">All properties being added</param>
        public void AddProperties(UnitPropertySuper[] properties)
        {
            foreach (UnitPropertySuper prop in properties)
                this.AddProperty(prop);
        }

        /// <summary>
        /// Adds new instances of each propertie passed in, then adds them too this PropertiesManager. Propertie settings are maintained!
        /// </summary>
        /// <param name="properties">All properties too add</param>
        /// <returns>All properties placed into the object(for further tweaking if needed)</returns>
        public UnitPropertySuper[] AddNewProperties(UnitPropertySuper[] properties)
        {
            List<UnitPropertySuper> result = new List<UnitPropertySuper>();
            foreach (UnitPropertySuper prop in properties)
            {
                UnitPropertySuper newProp = (UnitPropertySuper)this.gameObject.AddComponent(typeof(UnitPropertySuper));
                this.AddProperty(newProp);
                result.Add(newProp);
            }
            return result.ToArray();
        }

        /// <summary>
        /// Gets the first property that matches type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>The property, if none are found, null, O = n</returns>
        public T GetProperty<T>()
            where T : UnitPropertySuper
        {
            foreach (UnitPropertySuper property in unitProperties)
                if (property.GetType() == typeof(T))
                    return (T)property;

            return default(T);
        }

        /// <summary>
        /// Get all properties of the T
        /// </summary>
        /// <typeparam name="T">Type of property to get</typeparam>
        /// <returns>Returns an array of all the types matching T found, O = n</returns>
        public T[] GetProperties<T>()
            where T : UnitPropertySuper
        {
            List<T> result = new List<T>();
            foreach (UnitPropertySuper property in unitProperties)
                if (property.GetType() == typeof(T))
                    result.Add((T)property);

            return result.ToArray();
        }

        /// <summary>
        /// Attempts to remove the first property found matching type T
        /// </summary>
        /// <typeparam name="T">Type of property you want to remove</typeparam>
        /// <returns>Wheither or not the method was a succes or not, O = n</returns>
        public bool RemoveProperty<T>()
            where T : UnitPropertySuper
        {
            int length = unitProperties.Count;
            for (int i = 0; i < length; i++)
            {
                UnitPropertySuper prop = unitProperties[i];
                if (prop.GetType() == typeof(T))
                {
                    unitProperties.Remove(prop);
                    Destroy(prop);
                    return true;
                }
            }
            return false;                 
        }

        /// <summary>
        /// Removes the specific property passed in, if it is contained within this property manager
        /// </summary>
        /// <param name="propertyToRemove">Property to remove</param>
        /// <returns>Wheither or not the method was a succes or not, O = n</returns>
        public bool RemoveProperty(UnitPropertySuper propertyToRemove)
        {
            if (unitProperties.Contains(propertyToRemove))
            {
                unitProperties.Remove(propertyToRemove);
                Destroy(propertyToRemove);
                return true;
            }
            return false;
        }
    }
}
