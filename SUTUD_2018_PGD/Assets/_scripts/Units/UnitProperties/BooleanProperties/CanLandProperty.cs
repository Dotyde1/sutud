﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(IsOnLandProperty))]
    [AddComponentMenu("Properties/Boolean Properties/CanLandProperty")]
    public class CanLandProperty : BooleanPropertySuperclass
    {

    }
}