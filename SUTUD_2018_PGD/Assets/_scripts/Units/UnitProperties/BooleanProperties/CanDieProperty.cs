﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Properties/Boolean Properties/CanDieProperty")]
    public class CanDieProperty : BooleanPropertySuperclass
    {

    }
}