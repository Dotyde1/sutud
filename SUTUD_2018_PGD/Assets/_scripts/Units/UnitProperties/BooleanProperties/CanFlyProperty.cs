﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(IsFlyingProperty))]
    [AddComponentMenu("Properties/Boolean Properties/CanFlyProperty")]
    public class CanFlyProperty : BooleanPropertySuperclass
    {
    }
}
