﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(IsSwimmingProperty))]
    [AddComponentMenu("Properties/Boolean Properties/CanSwimProperty")]
    public class CanSwimProperty : BooleanPropertySuperclass
    {

    }
}