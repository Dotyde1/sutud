﻿using Sutud.Units.UnitStates;

namespace Sutud.Units.Properties
{
    public class UnitStateProperty : UnitProperty<UnitState>, IUnitProperty<UnitState>
    {
    }
}
