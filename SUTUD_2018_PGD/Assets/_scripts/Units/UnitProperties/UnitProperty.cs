﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [RequireComponent(typeof(UnitPropertiesManager))]
    public class UnitProperty<T> : UnitPropertySuper, IProperty
    {
        [SerializeField]
        T value;

        public T _value { get { return this.value; } set { this.value = value; } }

        public T GetComponentValue()
        {
            return value;
        }

        public void SetComponentValue(T value)
        {
            this.value = value;
        }
    }
}
