﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    /// <summary>
    /// Enum to hold all races know too the game, if you add a race too the game be sure to add it here!
    /// </summary>
    public enum Race { None, Human, all }

    //keep the "last" classification last for logic purposes
    public enum UnitClassifications { tank, ranged, physical, assassin, mage, melee, spellcaster, healer, buffer, debuffer, counterMage, antiMage, building, income, last };

    [System.Serializable]
    public class KeyValuePair<TKey, TValue>
    {

        public KeyValuePair()
        {
        }

        public KeyValuePair(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        public TKey Key { get; set; }
        public TValue Value { get; set; }

    }

    [System.Serializable]
    public struct ClassificationEntry
    {
        public UnitClassifications classification;
        [Tooltip("Negative values are not possible. If a negative value is passed it it will be made positive via the ABS method!")]
        public float signifiganceOfClassifiation;

        public ClassificationEntry(UnitClassifications classification, float signifiganceOfClassifiation)
        {
            this.classification = classification;
            this.signifiganceOfClassifiation = Mathf.Abs(signifiganceOfClassifiation);
        }

    }
}