﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    public class StringPropertySuperclass : UnitProperty<string>, IStringUnitProperty
    {
        [TextArea]
        string value;
    }
}
