﻿using UnityEngine;
namespace Sutud.Units.Properties
{
    [AddComponentMenu("Properties/String Properties/ToolTipProperty")]
    public class ToolTipProperty : StringPropertySuperclass { }
}