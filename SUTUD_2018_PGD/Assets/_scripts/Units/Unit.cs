﻿using Sutud.Units.Behaviors;
using Sutud.Units.Properties;
using Sutud.Units.UnitStates;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Sutud.Units
{
    [RequireComponent(typeof(OwnerIDProperty))]
    [RequireComponent(typeof(UnitStateProperty))]
    [RequireComponent(typeof(UnitPropertiesManager))]
    [RequireComponent(typeof(UnitBehaviourManager))]
    [RequireComponent(typeof(CanDieProperty))]
    [RequireComponent(typeof(WaveSpawnCountProperty))]
    public class Unit : MonoBehaviour
    {
        [SerializeField]
        public UnitPropertiesManager unitPropertiesManager;

        [SerializeField]
        public UnitBehaviourManager unitBehaviourManager;

        [SerializeField]
        private OwnerIDProperty ownerIdProperty;
        public PlayerManager myPlayer;

        [SerializeField]
        private Rigidbody rigidBody;

#if UNITY_EDITOR
        [SerializeField]
        private UnitState expectedUnitState;
        [SerializeField]
        bool demoScene = false;
#endif
        [SerializeField]
        public UnitStateProperty unitStateProperty;

        public Unit target;
        public Sprite UiIcon;

        public event EventHandler<UnitEventArg> OnChangedTarget;
        public event EventHandler<UnitEventArg> OnHit;
        public event EventHandler<UnitStateEventArg> OnStateChange;
        public event EventHandler<EventArgs> OnPurchase;

        void Start()
        {
#if UNITY_EDITOR
            if(demoScene)
                Init();
#endif
        }

        public void Init()
        {
            this.myPlayer = PlayersManagement.instance.FindPlayerByID(ownerIdProperty._value);
            this.unitPropertiesManager.Initialize();
			this.unitBehaviourManager.Initialize();
            if (myPlayer != null)
                myPlayer.aliveUnits.Add(this);
        }

#if UNITY_EDITOR
        public void Bake()
        {
            UnBake();
            this.rigidBody = GetComponent<Rigidbody>();
            this.unitPropertiesManager = gameObject.GetComponent<UnitPropertiesManager>();
            this.unitBehaviourManager = gameObject.GetComponent<UnitBehaviourManager>();
            this.unitPropertiesManager.Bake();
            this.unitBehaviourManager.Bake(this);

            this.unitStateProperty = this.unitPropertiesManager.GetProperty<UnitStateProperty>();
            this.ownerIdProperty = this.unitPropertiesManager.GetProperty<OwnerIDProperty>();
        }

        public void UnBake()
        {
            this.rigidBody = null;
            if (this.unitPropertiesManager != null)
                this.unitPropertiesManager.UnBake();
            if (this.unitBehaviourManager != null)
                this.unitBehaviourManager.UnBake();
            else
            {
                UnitBehaviour[] behaviors = this.GetComponents<UnitBehaviour>();
                foreach (UnitBehaviour behavior in behaviors)
                {
                    behavior.UnBake();
                }
            }

            this.unitPropertiesManager = null;
            this.unitBehaviourManager = null;

            this.unitStateProperty = null;
            this.ownerIdProperty = null;
        }
#endif

        void Update()
        {
#if UNITY_EDITOR
            this.CheckForStateChange();
#endif
            if (unitBehaviourManager.enabled)
                this.unitBehaviourManager.CustomUpdate();
            
            rigidBody.velocity = Vector3.zero;
        }

        private void FixedUpdate()
        {

            if (unitBehaviourManager.enabled)
                this.unitBehaviourManager.CustomFixedUpdate();
        }

        void OnDestroy()
        {
            try
            {
                if(myPlayer != null)
                    myPlayer.aliveUnits.Remove(this);
                if (unitBehaviourManager.enabled)
                    this.unitBehaviourManager.CustomOnDestroy();
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.StackTrace);
            }
        }

        [ContextMenu("DebugCallTargetChanged")]
        public void DebugCallTargetChanged()
        {
            OnChangedTarget(this, new UnitEventArg(this.target));
        }

        public void SetTargetUnit(Unit newTarget)
        {
            target = newTarget;
            OnChangedTarget(this, new UnitEventArg(newTarget));
        }

        public void UnitIsHit(Unit hitter)
        {
            if(OnHit != null){
				OnHit.Invoke(this, new UnitEventArg(hitter) );
				FleeingBehaviour.isFleeing = true;
			}
        }

        public void ChangeState(UnitState newState)
        {
#if UNITY_EDITOR
            expectedUnitState = newState;
#endif
            unitStateProperty._value = newState;
            OnStateChange(this, new UnitStateEventArg(newState));
        }

#if UNITY_EDITOR
        public void CheckForStateChange()
        {
            if (unitStateProperty._value != expectedUnitState)
                ChangeState(unitStateProperty._value);
        }
#endif

        public class UnitEventArg : EventArgs
        {
            public Unit unit;
            public UnitEventArg(Unit unit)
            {
                this.unit = unit;
            }
        }
        [ContextMenu("DebugOnPurchase")]
        public void DebugOnPurchase()
        {
            OnPurchase(this, new EventArgs());
        }

        public void JustPurchased()
        {
            if(OnPurchase != null)
            {
                OnPurchase(this, new EventArgs());
            }
        }

        public class UnitStateEventArg : EventArgs
        {
            public UnitState state;
            public UnitStateEventArg(UnitState state)
            {
                this.state = state;
            }
        }
    }
}
