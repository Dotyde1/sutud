﻿using UnityEngine;

// public unit reference
namespace Sutud.Units
{
    public class DecalColorGetter : MonoBehaviour
    {
        public Unit unit;

        public Color marketUnitColor = Color.gray;

        // Use this for initialization
        void Start()
        {
            unit = GetComponentInParent<Unit>();
            Renderer rend = GetComponent<Renderer>();
            rend.material.color = marketUnitColor;
        }

        // Update is called once per frame
        void Update()
        {
            if(unit.myPlayer != null)
            {
                Renderer rend = GetComponent<Renderer>();
                rend.material.color = unit.myPlayer.color;
                this.enabled = false;
            }
        }
    }
}


