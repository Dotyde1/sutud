﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnHitBecomeRandomColor : MonoBehaviour, IHitable
{
    MeshRenderer myRenderer;

    private void Start()
    {
        myRenderer = GetComponent<MeshRenderer>();
    }

    public void Hit(GameObject source)
    {
        myRenderer.material.color = GetRandomColor();
    }

    public Color GetRandomColor()
    {
        return new Color(Random.value, Random.value, Random.value);
    }
}
