﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors
{
    [RequireComponent(typeof(SightDistanceProperty))]
    public class FindEnemyBehavior : UnitBehaviour
    {
        [SerializeField]
        List<Unit> DetectedEnemies = new List<Unit>();
        SightDistanceProperty sightDistance;

        private void Start()
        {

        }

        public override void CustomFixedUpdate()
        {
            if (sightDistance == null)
            {
                sightDistance = unit.unitPropertiesManager.GetProperty<SightDistanceProperty>();
            }

            DetectedEnemies = UpdateDetectedEnemiesList();
            Unit foundUnit = GetHighestAggroUnit(DetectedEnemies);
            if (foundUnit != null)
            {
                unit.SetTargetUnit(foundUnit);
            }

        }

        public override void CustomOnDestroy()
        {

        }

        public override void CustomUpdate()
        {
        }

        public List<Unit> UpdateDetectedEnemiesList()
        {
            List<Unit> unitsDetected = new List<Unit>();
            foreach (PlayerManager opponent in unit.myPlayer.opponents)
            {
                foreach (Unit opponentUnit in opponent.aliveUnits)
                {
                    if (Vector3.Distance(opponentUnit.transform.position, this.transform.position) < sightDistance._value)
                    {
                        unitsDetected.Add(opponentUnit);
                    }
                }
            }
            return unitsDetected;
        }

        public virtual Unit GetHighestAggroUnit(List<Unit> units)
        {
            if (units.Count == 0)
                return null;
            Unit highestAggroUnit = null;
            float highestFoundAggro = float.MinValue;
            foreach (Unit unit in units)
            {
                AggroProperty aggroPropFound = unit.unitPropertiesManager.GetProperty<AggroProperty>();

                if (aggroPropFound == null)
                {
                    if (highestAggroUnit == null)
                        highestAggroUnit = unit;
                }
                else
                {
                    if (highestFoundAggro < aggroPropFound._value)
                    {
                        highestFoundAggro = aggroPropFound._value;
                        highestAggroUnit = unit;
                    }
                }
            }

            if (this.unit.target == null)
                return highestAggroUnit;
            //Debug.Log("target " + this.unit.target.name);
            if (this.unit.target.unitPropertiesManager.GetProperty<AggroProperty>()._value * 1.5 > highestFoundAggro)
                return highestAggroUnit;
            else
                return null;

        }
    }
}
