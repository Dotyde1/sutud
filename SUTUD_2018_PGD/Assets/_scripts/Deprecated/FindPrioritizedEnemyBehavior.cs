﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.Properties;

namespace Sutud.Units.Behaviors
{

    public class FindPrioritizedEnemyBehavior : FindEnemyBehavior
    {

        public List<ClassificationEntry> priorityList = new List<ClassificationEntry>();

        public override Unit GetHighestAggroUnit(List<Unit> units)
        {
            if (units.Count == 0)
                return null;

            Unit highestAggroUnit = null;
            float highestFoundAggro = float.MinValue;
            foreach (Unit enemyUnit in units)
            {
                AggroProperty aggroPropFound = enemyUnit.unitPropertiesManager.GetProperty<AggroProperty>();

                if (aggroPropFound == null)
                {
                    if (highestAggroUnit == null)
                        highestAggroUnit = enemyUnit;
                }
                else
                {
                    float aggroFound = aggroPropFound._value;
                    if (aggroFound == 0)
                    {
                        aggroFound = 1;
                    }

                    ClassificationProperty[] targetClassifications = enemyUnit.unitPropertiesManager.GetProperties<ClassificationProperty>();
                    float priorityAggro = 0;
                    foreach (ClassificationProperty targetClassProp in targetClassifications)
                    {
                        foreach (ClassificationEntry entry in priorityList)
                        {
                            if (entry.classification == targetClassProp._value.classification)
                            {
                                priorityAggro += aggroFound * entry.signifiganceOfClassifiation * targetClassProp._value.signifiganceOfClassifiation;
                            }
                        }
                    }

                    if (aggroPropFound._value == 0)
                    {
                        aggroFound -= 1;
                    }

                    aggroFound /= Vector3.Distance(this.transform.position, enemyUnit.transform.position);

                    if (highestFoundAggro < aggroFound + priorityAggro)
                    {
                        highestFoundAggro = aggroFound;
                        highestAggroUnit = enemyUnit;
                    }
                }
            }

            if (this.unit.target == null)
                return highestAggroUnit;
            //Debug.Log("target " + this.unit.target.name);
            if (this.unit.target.unitPropertiesManager.GetProperty<AggroProperty>()._value * 1.5 > highestFoundAggro)
                return highestAggroUnit;
            else
                return null;
        }

    }
}
