﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sutud.Cameras;

public class NoCameraSnapPointFoundException : Exception {
    public NoCameraSnapPointFoundException(CameraSnapNames snap, int playerID) : base(GenerateMessage(snap, playerID)) 
    {
    }

    private static string GenerateMessage(CameraSnapNames snap, int playerID) {
        return string.Format("Could not find the camera snap of type {0} for player {1}", snap, playerID); ;
    }
}
