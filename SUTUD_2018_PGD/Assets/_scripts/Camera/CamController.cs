﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Sutud.Cameras
{
    public class CamController : MonoBehaviour
    {
        public CameraSnap MyCameraSnapper;

        public int Speed = 20;

        [Tooltip("Needs an high number to be usefull")]
        public int scrollSpeed = 500;
        public int minY = 10;
        public int maxY = 30;
        public float panBorderThickness = 10f;
        [Tooltip("Y is actually Z")]
        public Vector2 panlimit = new Vector2(20f, 20f);
        public int RotateSpeed = 50;
        [SerializeField]
        private GameObject RotationObject;
        private float speedmodifier;
        public bool invertedscroll = false;
        float scroll = 0;
        bool moving;
        public int playerID = 0;

        // Update is called once per frame
        void Update()
        {
            Vector3 pos = transform.position;
            Vector3 velocity = Vector3.zero;
            velocity.x += Input.GetAxis("Horizontal");
            velocity.z += Input.GetAxis("Vertical");
            Vector3 mousePosition = Input.mousePosition;

            // mouse movement
            if (mousePosition.y < Screen.height && mousePosition.y > 0)
            {
                if (mousePosition.y >= Screen.height - panBorderThickness)
                {
                    float DepthInPanBorder = mousePosition.y - (Screen.height - panBorderThickness);
                    velocity.z += DepthInPanBorder / panBorderThickness;
                }
                if (mousePosition.y <= panBorderThickness)
                {
                    float DepthInPanBorder = mousePosition.y - panBorderThickness;
                    velocity.z += DepthInPanBorder / panBorderThickness;
                }
            }
            if (mousePosition.x < Screen.width && mousePosition.x > 0)
            {
                if (mousePosition.x >= Screen.width - panBorderThickness)
                {

                    float DepthInPanBorder = mousePosition.x - (Screen.width - panBorderThickness);
                    velocity.x += DepthInPanBorder / panBorderThickness;
                }
                if (mousePosition.x <= panBorderThickness)
                {
                    float DepthInPanBorder = mousePosition.x - panBorderThickness;
                    velocity.x += DepthInPanBorder / panBorderThickness;
                }
            }

            if (Input.GetAxis("Rotate") != 0)
            {
                Rotate();
            }
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
               velocity = Scroll(velocity,pos);
            }

            pos += transform.rotation * (velocity * Speed * Time.deltaTime);


            if (velocity.sqrMagnitude != 0)
            {
                MyCameraSnapper.StopMoving();
            }
            // Keeps the camera withing the set area
            pos.x = Mathf.Clamp(pos.x, -panlimit.x, panlimit.x);
            pos.z = Mathf.Clamp(pos.z, -panlimit.y, panlimit.y);
            pos.y = Mathf.Clamp(pos.y, minY, maxY);

            transform.position = pos;



        }
        //Scrolling
        public Vector3 Scroll(Vector3 velocity, Vector3 pos)
        {
            //scrolling
            scroll = invertedscroll ? -Input.GetAxis("Mouse ScrollWheel") : Input.GetAxis("Mouse ScrollWheel");
            velocity.y += scroll * Time.deltaTime * -scrollSpeed;
            //keeps camera withing boundary
            if (transform.position.y >= minY + 1 && transform.position.y <= maxY - 1)
            {
                velocity.z += scroll * Time.deltaTime * scrollSpeed;
            }
            return velocity;
        }
        //Rotation
        public void Rotate()
        {
            transform.RotateAround(RotationObject.transform.position, Vector3.up, Input.GetAxis("Rotate") * RotateSpeed * Time.deltaTime);
        }
    }


    

}

