﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Variables.RuntimeSets;
namespace Sutud.Cameras
{
    public class CameraPoint : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("The snapping type I am used for")]
        CameraSnapNames mySnap;
        [SerializeField]
        [Tooltip("The player ID I belong too")]
        int playerID;

        public CameraSnapNames SnapName
        {
            get
            {
                return mySnap;
            }
        }

        public int PlayerID
        {
            get
            {
                return playerID;
            }
        }

        public CameraPointRunTimeSet setToAddThis;
        public void OnEnable() { setToAddThis.Add(this); }
        public void OnDisable() { setToAddThis.Remove(this); }
    }
}
