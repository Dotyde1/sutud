﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sutud.Events;

public class CountDownTimer : MonoBehaviour {
	private float timeRemaining;

    public float totalTime = 30;

	public string playingText = "Start";
	[SerializeField]
	private Text timerText;
	[SerializeField]
	private GameEvent startWave;

    private bool startText = true;
	// Use this for initialization
	void Start () {
        EndWave();
    }

	// Update is called once per frame
	void Update () {
        if (startText) {
            timerText.text = "Time: " + (int)timeRemaining;
        }
		if (timeRemaining <= 0) {
			timeRemaining = totalTime;
			startWave.Raise();
		}
	}
	IEnumerator Timer(){
		while (true) {
			yield return new WaitForSeconds (1);
			timeRemaining--;
		}
	}
	public void EndWave(){
		timeRemaining = totalTime;
        startText = true;
		StartCoroutine (Timer());
	}

	public void EarlyWave(){
        //StopCoroutine(Timer());
        StopAllCoroutines();
        timeRemaining = totalTime;
        startText = false;
		timerText.text = playingText;
	}
}
