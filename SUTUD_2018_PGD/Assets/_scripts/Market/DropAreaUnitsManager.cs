﻿using Sutud.Units;
using Sutud.Units.Properties;
using Sutud.Variables;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.UnitStates;

public class DropAreaUnitsManager : MonoBehaviour
{
    public int myPlayerID;
    public Transform spawnAreaTransform;
    public List<GameObject> myUnits = new List<GameObject>();
    public UnitState playingState;

    private void Start()
    {
        PlayersManagement.instance.FindPlayerByID(myPlayerID).myDropArea = this;
    }

    private void FixedUpdate()
    {
    }

    public void Add(GameObject unit)
    {
        this.myUnits.Add(unit);
        var unitScript = unit.GetComponent<Unit>();

        unitScript.unitPropertiesManager.GetProperty<OwnerIDProperty>()._value = myPlayerID;
        unitScript.myPlayer = PlayersManagement.instance.FindPlayerByID(myPlayerID);
        unitScript.JustPurchased();
        int spawnCount = unitScript.unitPropertiesManager.GetProperty<WaveSpawnCountProperty>()._value;
        for (int i = 0; i < spawnCount; i++)
        {
            unitScript.myPlayer.spawnUnits.Add(unitScript);
        }
        //unit.name = unit.name.Remove(0, 12);

        unit.name += string.Format(" Owner: {0}, UnitNumber: {1}", myPlayerID, myUnits.Count);
    }

    [ContextMenu("Send wave!")]
    public void SendWave()
    {
        Vector3 deltaPos = this.transform.position - spawnAreaTransform.position;
        foreach (GameObject unit in myUnits)
        {
            var newUnitPreSpawn = unit.GetComponent<Unit>();
            if(newUnitPreSpawn != null){
                int numberToSpawn = 0;
                try
                {
                    numberToSpawn = newUnitPreSpawn.GetComponent<Unit>().unitPropertiesManager.GetProperty<WaveSpawnCountProperty>()._value;
                }
                catch(System.NullReferenceException e)
                {
                    Debug.LogWarning("Unit in spawn area does not contain a waveSpawnCount property, this is a required property!\n" + e.StackTrace);
                }
                for(int i = 0; i < numberToSpawn; i++)
                {
                    var newUnit = Instantiate(unit);
                    newUnit.transform.position -= deltaPos;
                    newUnit.GetComponent<Rigidbody>().isKinematic = false;
                    var unitComponent = newUnit.GetComponent<Unit>();
                    var dragNDrop = newUnit.GetComponent<DragAndDropUnit>();
                    if (dragNDrop != null)
                        Destroy(dragNDrop);//remove the dragn drop tool
                    if(unitComponent != null)
                    {
                        unitComponent.Init();
                        unitComponent.ChangeState(playingState);
                        var navmeshagent = newUnit.GetComponent<UnityEngine.AI.NavMeshAgent>();
                        navmeshagent.enabled = false;
                        navmeshagent.enabled = true;
                    }
                }
            }
        }
    }

}
