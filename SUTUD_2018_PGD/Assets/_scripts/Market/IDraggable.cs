﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDraggable
{

    void Drag(GameObject gameObject, Vector3 position);
    void Drop();
    GameObject GetGameObject();
    void Reset();
    void StartDrag();
}
