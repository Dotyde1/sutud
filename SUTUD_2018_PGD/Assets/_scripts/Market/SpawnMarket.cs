﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Sutud.Units.Properties;
using Sutud.Units;
using Sutud.AiCore;

public class SpawnMarket : MonoBehaviour
{
    public GameObject aiCorePrefab;
    public GameObject platform;

    public List<GameObject> units = new List<GameObject>();

    public Race race;

    public GameObject altarModel;
    public float seperationDistance = 3;

    public int shopForPlayerID = 0;
    private PlayerManager shopForPlayer;
    
	void Start ()
    {
        shopForPlayer = PlayersManagement.instance.FindPlayerByID(shopForPlayerID);

        if(shopForPlayer == null)
        {
            Debug.LogWarning("Shop found no player! player ID searched for was: " + shopForPlayerID);
            Destroy(this.gameObject);
            return;
        }

        var unitsDictonary = RaceLoader.instance.Units;

        if (!unitsDictonary.ContainsKey(race))
        {
            return;
        }

        units = unitsDictonary[race].ToList();
        units.AddRange(unitsDictonary[Race.all]);
        shopForPlayer.unitsInShop = units;
        if (shopForPlayer.isAi)
        {
            var aiCoreInstance = Instantiate(aiCorePrefab).GetComponent<AiCore>();
            aiCoreInstance.Initialize(shopForPlayer, units, platform, platform.GetComponent<DropAreaUnitsManager>());
#if UNITY_EDITOR
            aiCoreInstance.name += shopForPlayerID;
#endif
            //No need to summon the altars for the AI, leave that crud empty :)
            return;
        }

        //int round is on prupose
        int halfCount = units.Count / 2;

        for(int i = 0; i < units.Count; i++)
        {
            var unit = units[i];

            var globalAltarObject = new GameObject();
            globalAltarObject.transform.parent = this.transform;
#if UNITY_EDITOR
            globalAltarObject.name = "Altar " + unit.name;
#endif

            var possitionToSpawnShit = this.transform.position;

            var newAltar = Instantiate(altarModel, globalAltarObject.transform);

            var newUnit = Instantiate(unit, globalAltarObject.transform);

            newUnit.transform.rotation = this.transform.rotation;
            newUnit.GetComponent<Unit>().Init();

            possitionToSpawnShit += this.transform.right * ((newAltar.GetComponent<Renderer>().bounds.size.x + seperationDistance) * (i - halfCount));

            newAltar.transform.position = possitionToSpawnShit;

            possitionToSpawnShit.y += newAltar.GetComponentInChildren<Renderer>().bounds.size.y / 2;
            //newUnit.transform.position = globalAltarObject.transform.position;
            newUnit.transform.position += possitionToSpawnShit;
        }
	}
}
