﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDropUnit : MonoBehaviour, IDraggable
{
    public Vector3 pickupLocation;
    protected Rigidbody rigidBody;

    private void Start()
    {
    }

    public void StartDrag()
    {
        pickupLocation = this.gameObject.transform.position;
    }

    public virtual void Drag(GameObject source, Vector3 position)
    {
        this.transform.position = new Vector3(position.x, position.y, position.z);
    }

    public virtual void Drop()
    {
    }

    public virtual GameObject GetGameObject()
    {
        return this.gameObject;
    }

    protected void SetKinemetic(bool value)
    {
        if(rigidBody == null)
        {
            rigidBody = GetComponent<Rigidbody>();
            rigidBody.isKinematic = value;
        }
        else
        {
            rigidBody.isKinematic = value;
        }
    }

    public virtual void Reset()
    {
        this.transform.position = pickupLocation;
    }
}
