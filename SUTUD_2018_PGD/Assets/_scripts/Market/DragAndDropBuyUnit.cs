﻿using Sutud.Units;
using Sutud.Units.Properties;
using UnityEngine;

[RequireComponent(typeof(OwnerIDProperty))]
[RequireComponent(typeof(CostProperty))]
public class DragAndDropBuyUnit : DragAndDropUnit, IDraggable
{
    bool bought = false;

    private void Start()
    {
        pickupLocation = this.gameObject.transform.position;
    }

    public virtual bool AttemptPurchase(int id)
    {
        bool result = false;
        Unit u = this.GetComponent<Unit>();
        var pms = PlayersManagement.instance;
        PlayerManager pm = pms.FindPlayerByID(id);
        if (pm != null)
        {
            // check enough money
            CostProperty cost = u.unitPropertiesManager.GetProperty<CostProperty>();

            if (pm.gold >= cost._value)
            {
                pm.gold -= cost._value;
                result = true;

                SetKinemetic(false);
                var costIncrease = u.unitPropertiesManager.GetProperty<CostIncreaseProperty>();
                if (costIncrease != null)
                {
                    cost._value += costIncrease._value;
                }
                Debug.Log("Bought for " + cost._value);
                pm.numberofunits++;
            }
            else if (bought == false)
            {
                OnNotEnoughFunds(u, u.myPlayer);
                Debug.Log("Not enough gold");
            }
        }
        return result;
    }

    private void OnNotEnoughFunds(Unit u, PlayerManager p)
    {

    }

    public override void Reset()
    {
        base.Reset();
        SetKinemetic(true);
    }

    public override void Drop()
    {
        base.Drop();
    }
}
