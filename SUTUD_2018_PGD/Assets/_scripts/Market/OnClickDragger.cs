﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units;
using Sutud.Units.UnitStates;

[RequireComponent(typeof(Camera))]
public class OnClickDragger : MonoBehaviour {

    Camera myCamera;
    IDraggable draggableObject;

    public LayerMask dragMask;
    public LayerMask dropMask;
    public UnitState spawnAreaUnitState;

    // Use this for initialization
    void Start () {
        myCamera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                var hitable = hit.transform.gameObject.GetComponent<IDraggable>();
                if (hitable != null)
                {
                    this.draggableObject = hitable;
                    hitable.StartDrag();
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if(this.draggableObject != null)
            {
                RaycastHit hit;
                Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
                var marketUnit = draggableObject.GetGameObject().GetComponent<DragAndDropUnit>();

                if (Physics.Raycast(ray, out hit, 100.0f, dropMask))
                {
                    if (marketUnit != null)
                    {
                        var dropAreaManager = hit.transform.gameObject.GetComponent<DropAreaUnitsManager>();
                        var dragAndDropBuyUnit = marketUnit as DragAndDropBuyUnit;
                        if (dragAndDropBuyUnit != null && (dropAreaManager != null && dragAndDropBuyUnit.AttemptPurchase(dropAreaManager.myPlayerID)))
                        {
                            var newPlayerUnit = Instantiate(this.draggableObject.GetGameObject());

                            newPlayerUnit.transform.position = marketUnit.transform.position;
                            newPlayerUnit.name = newPlayerUnit.name.Replace("(Clone)", "");
                            //Set Owner id.

                            Destroy(newPlayerUnit.GetComponent<DragAndDropBuyUnit>()); //disable purchase possibility
                            newPlayerUnit.AddComponent<DragAndDropUnit>(); //make it still dragable

                            // set unit to in spawn area state
                            newPlayerUnit.GetComponent<Unit>().Init();
                            newPlayerUnit.GetComponent<Unit>().ChangeState(spawnAreaUnitState);

                            dropAreaManager.Add(newPlayerUnit);

                            dragAndDropBuyUnit.Reset();
                        }
                        else if(marketUnit != null)
                        {
                            marketUnit.Reset();
                            this.draggableObject = null;
                        }
                    }
                    this.draggableObject = null;
                }
                else if (marketUnit != null)
                {
                    marketUnit.Reset();
                    draggableObject = null;
                }
            }
        }
        else
        {
            if (this.draggableObject != null)
            {
                RaycastHit hit;
                Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 100.0f, dragMask))
                {
                    this.draggableObject.Drag(this.gameObject, hit.point);
                }
            }
        }
    }
}
