﻿using System;
using System.Collections;
using System.Collections.Generic;

using Sutud.Units.Properties;
using UnityEngine;

namespace Sutud.AiCore.UnitSelectionModules
{
    public abstract class AiUnitSelectionModule : ScriptableObject
    {
        public string moduleName;

        internal abstract AiCore.AiDecisions MakeDecision(
            Dictionary<GameObject, UnitData> analizedUnitDictionary,
            PlayerManager playerManagerReference, 
            Units.Properties.KeyValuePair<GameObject, UnitData> cheapestUnit, 
            out GameObject unitToPurchase, 
            out UnitData unitCostProperty);

        protected List<GameObject> GetAffordableUnits(Dictionary<GameObject, UnitData> analizedUnitDictionary, PlayerManager playerManagerReference)
        {
            List<GameObject> affordableUnits = new List<GameObject>();
            foreach(var unit in analizedUnitDictionary.Keys)
            {
                if(analizedUnitDictionary[unit].costProperty._value < playerManagerReference.gold)
                    affordableUnits.Add(unit);
            }
            return affordableUnits;
        }
    }
}
