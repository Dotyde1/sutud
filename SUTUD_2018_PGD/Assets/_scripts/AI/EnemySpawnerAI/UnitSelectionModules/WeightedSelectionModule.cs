﻿using System.Collections;
using System.Collections.Generic;
using Sutud.Units.Properties;
using UnityEngine;
using Sutud.Variables;

namespace Sutud.AiCore.UnitSelectionModules
{
    [CreateAssetMenu(menuName = "AIUnitSelectionModules/WheightedSelectionModule")]
    public class WeightedSelectionModule : AiUnitSelectionModule
    {
        public List<ClassificationEntry> favoredClasificationEntries;

        struct UnitPointsStruct
        {
            public FloatRange points;
            public GameObject unit;
            public UnitPointsStruct(FloatRange points, GameObject unit)
            {
                this.points = points;
                this.unit = unit;
            }
        }

        internal override AiCore.AiDecisions MakeDecision(Dictionary<GameObject, UnitData> analizedUnitDictionary, PlayerManager playerManagerReference, 
            Units.Properties.KeyValuePair<GameObject, UnitData> cheapestUnit, 
            out GameObject unitToPurchase, out UnitData unitCostProperty)
        {
            List<GameObject> affordableUnits = GetAffordableUnits(analizedUnitDictionary, playerManagerReference);

            if(affordableUnits.Count == 0)
            {
                //implement saving for behavior
                unitToPurchase = null;
                unitCostProperty = new UnitData();
                return AiCore.AiDecisions.DoNothing;
            }
            
            float totalPoints = 0;
            List<UnitPointsStruct> pointsStructList = new List<UnitPointsStruct>();
            foreach(var unit in affordableUnits)
            {
                float points = analizedUnitDictionary[unit].costProperty._value;

                foreach(var unitClassification in analizedUnitDictionary[unit].classifications)
                {
                    foreach(var favordClassification in favoredClasificationEntries)
                    {
                        if(unitClassification._value.classification == favordClassification.classification)
                        {
                            points += analizedUnitDictionary[unit].costProperty._value * unitClassification._value.signifiganceOfClassifiation * favordClassification.signifiganceOfClassifiation;
                        }
                    }
                }

                pointsStructList.Add(new UnitPointsStruct(new FloatRange(totalPoints, totalPoints + points), unit));

                totalPoints += points;
            }

            float unitPointsToPurchase = Random.Range(0f, totalPoints);
            foreach(var pointsStructThing in pointsStructList)
            {
                if (pointsStructThing.points.IsInRange(unitPointsToPurchase))
                {
                    unitToPurchase = pointsStructThing.unit;
                    unitCostProperty = analizedUnitDictionary[unitToPurchase];
                    return AiCore.AiDecisions.PurchaseUnit;
                }
            }
            unitToPurchase = affordableUnits[0];
            unitCostProperty = analizedUnitDictionary[unitToPurchase];
            return AiCore.AiDecisions.PurchaseUnit;
        }
    }
}
