﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sutud.Units.Properties;
using System;

namespace Sutud.AiCore.UnitSelectionModules
{
    [CreateAssetMenu(menuName = "AIUnitSelectionModules/PureChaos")]
    public class PureChaosSelection : AiUnitSelectionModule
    {
        internal override AiCore.AiDecisions MakeDecision(Dictionary<GameObject, UnitData> analizedUnitDictionary, 
            PlayerManager playerManagerReference, Sutud.Units.Properties.KeyValuePair<GameObject, UnitData> cheapestUnit, 
            out GameObject unitToPurchase, out UnitData unitCostProperty)
        {
            List<GameObject> affordableUnits = GetAffordableUnits(analizedUnitDictionary, playerManagerReference);

            if(affordableUnits.Count == 0)
            {
                unitToPurchase = null;
                unitCostProperty = new UnitData();
                return AiCore.AiDecisions.DoNothing;
            }

            unitToPurchase = affordableUnits.GetRandomFromList();
            unitCostProperty = analizedUnitDictionary[unitToPurchase];
            return AiCore.AiDecisions.PurchaseUnit;
        }
    }
}
