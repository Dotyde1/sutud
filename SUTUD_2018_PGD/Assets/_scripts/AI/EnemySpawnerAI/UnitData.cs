﻿using Sutud.Units;
using Sutud.Units.Properties;
using Sutud.Units.Weapons;
using Sutud.Units.Behaviors;

namespace Sutud.AiCore
{
    public struct UnitData
    {
        public Unit unit;
        public Weapon[] weapons;
        public CostProperty costProperty;
        public ClassificationProperty[] classifications;

        public UnitData(Unit unit)
        {
            this.unit = unit;
            UseWeaponsBehavior weaponbehavior = unit.unitBehaviourManager.GetBehaviour<UseWeaponsBehavior>();
            if (weaponbehavior != null)
                weapons = weaponbehavior.Weapons;
            else
                weapons = null;
            costProperty = unit.unitPropertiesManager.GetProperty<CostProperty>();
            classifications = unit.unitPropertiesManager.GetProperties<ClassificationProperty>();
        }
    }
}
