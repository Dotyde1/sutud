﻿using System.Collections.Generic;
using Sutud.AiCore.UnitPlacementModules;
using UnityEngine;
using Sutud.Units;
using Sutud.Units.Properties;
using Sutud.AiCore.UnitSelectionModules;
using Sutud.Units.UnitStates;

namespace Sutud.AiCore
{

    public class AiCore : MonoBehaviour
    {
        public enum AiDecisions
        {
            PurchaseUnit, DoNothing, SaveForUnit
        }

        PlayerManager playerManagerReference;

        public List<AiUnitSelectionModule> possibleModules;
        public AiUnitSelectionModule selectedModule;

        public List<AiUnitPlacementModule> possiblePlacementModules;
        protected AiUnitPlacementModule selectedPlacementodule;

        public GameObject platform;
        public UnitState spawnAreaUnitState;
        public DropAreaUnitsManager dropAreaManager;

        public List<GameObject> unitsInShop;
        public List<Unit> detectedEnemieUnits;

        public Units.Properties.KeyValuePair<GameObject, UnitData> cheapestUnit;
        public Units.Properties.KeyValuePair<GameObject, UnitData> savingForUnit;

        public AiDecisions decisionType;

        /// <summary>
        /// The analized unit dictionary
        /// For easier acces to frequently used variables & references
        /// </summary>
        protected Dictionary<GameObject, UnitData> analizedUnitDictionary;


        // Debug vars
        public GameObject MostRecentPurchasedUnit;

        internal void Initialize(PlayerManager playerManagerReference, List<GameObject> units, GameObject platform, DropAreaUnitsManager dropAreaManager)
        {
            this.platform = platform;
            this.dropAreaManager = dropAreaManager;

            this.playerManagerReference = playerManagerReference;
            unitsInShop = units;

            analizedUnitDictionary = AnaliseUnits(unitsInShop);

            selectedModule = possibleModules.GetRandomFromList();
            gameObject.name = "AiCore " + selectedModule.moduleName + " ID:";

            selectedPlacementodule = possiblePlacementModules.GetRandomFromList();
            selectedPlacementodule.SetPlatform(platform);

            StartUnitPurchasing();
        }

        protected Dictionary<GameObject, UnitData> AnaliseUnits(List<GameObject> unitsInShop)
        {
            var unitDataDictionary = new Dictionary<GameObject, UnitData>();

            foreach (var unitsGameObjects in unitsInShop)
            {
                var unitRef = unitsGameObjects.GetComponent<Unit>();
                unitDataDictionary.Add(unitsGameObjects, new UnitData(unitRef));
            }

            return unitDataDictionary;
        }

        public void StartUnitPurchasing()
        {
            if (!((savingForUnit != null) && (savingForUnit.Value.costProperty._value < playerManagerReference.gold)))
            {
                if (savingForUnit != null)
                {
                    PurchaseUnit(savingForUnit.Key, savingForUnit.Value);
                    savingForUnit = null;
                }

                cheapestUnit = FindCheapestUnit(analizedUnitDictionary);

                while (cheapestUnit.Value.costProperty._value < playerManagerReference.gold)
                {
                    GameObject unitToPurchase;
                    UnitData unitData;
                    decisionType = selectedModule.MakeDecision(analizedUnitDictionary, playerManagerReference,
                        cheapestUnit, out unitToPurchase, out unitData);

                    switch (decisionType)
                    {
                        case AiDecisions.DoNothing:
                            LogFinalizedUnitPurchasing();
                            return;
                        case AiDecisions.PurchaseUnit:
                            if (!(unitData.costProperty._value > playerManagerReference.gold))
                            {
                                PurchaseUnit(unitToPurchase, unitData);
                            }
                            else
                            {
                                // if we choose to purchase a unit we can't affor(somehow) instead start saving for it
                                goto case AiDecisions.SaveForUnit;
                            }
                            break;
                        case AiDecisions.SaveForUnit:
                            savingForUnit = new Units.Properties.KeyValuePair<GameObject, UnitData>(
                                unitToPurchase, unitData);
                            LogFinalizedUnitPurchasing();
                            return;
                        default:
                            Debug.Log("Some new aiDecision type was added but no behavior for it has been made. " +
                                      "Breaking out of the while loop for you to prevent unity crash.");
                            LogFinalizedUnitPurchasing();
                            return;
                    }
                    cheapestUnit = FindCheapestUnit(analizedUnitDictionary);
                }
            }

            LogFinalizedUnitPurchasing();
        }

        private void LogFinalizedUnitPurchasing()
        {
            Debug.Log(this.name + " Finalized unit purchasing");
        }

        private Units.Properties.KeyValuePair<GameObject, UnitData> FindCheapestUnit(Dictionary<GameObject, UnitData> unitDictionary)
        {
            Units.Properties.KeyValuePair <GameObject, UnitData> result = null;
            foreach (var unit in unitDictionary.Keys)
            {
                if(result == null)
                {
                    result = new Units.Properties.KeyValuePair<GameObject, UnitData>(unit, unitDictionary[unit]);
                }
                else
                {
                    if(result.Value.costProperty._value > unitDictionary[unit].costProperty._value)
                        result = new Units.Properties.KeyValuePair<GameObject, UnitData>(unit, unitDictionary[unit]);
                }
            }
            return result;
        }

        public void PurchaseUnit(GameObject unit, UnitData unitData)
        {
            playerManagerReference.gold -= unitData.costProperty._value;
            var costIncrease = unitData.unit.unitPropertiesManager.GetProperty<CostIncreaseProperty>();
            if (costIncrease != null)
            {
                unitData.costProperty._value += costIncrease._value;
            }
            MostRecentPurchasedUnit = unit;
            InstantiateAndPlaceUnit(unit);
        }

        public void UpdateDetectedEnemieUnits()
        {
            detectedEnemieUnits = new List<Unit>();
            foreach(var player in playerManagerReference.opponents)
            {
                detectedEnemieUnits.AddRange(player.spawnUnits);
            }
        }

        public void InstantiateAndPlaceUnit(GameObject unit)
        {
            GameObject newUnit = Instantiate(unit);

            Destroy(newUnit.GetComponent<DragAndDropBuyUnit>());

            // set unit to in spawn area state
            newUnit.GetComponent<Unit>().Init();
            newUnit.GetComponent<Unit>().ChangeState(spawnAreaUnitState);

            dropAreaManager.Add(newUnit);

            selectedPlacementodule.PlaceUnit(newUnit);
            Debug.Log("Building a " + unit.name);
        }

    }
}
