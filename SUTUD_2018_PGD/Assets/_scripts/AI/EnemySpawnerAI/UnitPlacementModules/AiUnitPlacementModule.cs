﻿using UnityEngine;

namespace Sutud.AiCore.UnitPlacementModules
{
    public abstract class AiUnitPlacementModule : ScriptableObject
    {
        internal GameObject platform;
        internal Bounds bounds;

        public float padding = 3;

        internal Vector3 max;
        internal Vector3 min;

        internal Vector3 range;

        internal void SetPlatform(GameObject platform)
        {
            this.platform = platform;
            bounds = this.platform.GetComponent<MeshRenderer>().bounds;
            max = bounds.max;
            min = bounds.min;

//            Debug.Log("Unit Placement min, max: " + min + ", " + max);

            max -= new Vector3(padding, padding, padding);
            min += new Vector3(padding, padding, padding);

//            Debug.Log("Unit Placement min, max: " + min + ", " + max);

            range = new Vector3(max.x - min.x, 0, max.z - min.z);

//            Debug.Log("range: " + range);
        }

        internal abstract void PlaceUnit(GameObject unit);
    }
}
