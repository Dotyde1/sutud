﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Sutud.AiCore.UnitPlacementModules
{
    [CreateAssetMenu(menuName = "AIUnitPlacementModules/PureChaos")]
    class PureChaosPlacement : AiUnitPlacementModule
    {
        internal override void PlaceUnit(GameObject unit)
        {
            Vector3 position = new Vector3(Random.Range(0f, range.x), bounds.max.y, Random.Range(0f, range.z));
            position += new Vector3(min.x, 0, min.z);

            Debug.Log("Unit position; " + position);

            unit.transform.position = position;
        }
    }
}
